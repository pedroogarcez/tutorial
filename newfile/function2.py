import numpy as np
#Developing a function to receive the temperature between the index

def mean(index1,index2,air,lats,lons):

	suma_air=np.zeros([len(lats),len(lons)])
	for i in range(index1,index2):
		suma_air[:,:] = suma_air[:,:] + air[i,:,:]
	media = suma_air[:,:]/(index2-index1)
	return media[:,:]
	

def meanp(air):	
	m= air.shape
	#m=[time[index1:index2],lat,lons]
	#m=[len(0),len(1),len(2)]

	suma_air=np.zeros([m[1],m[2]])
	
	for i in range(0,m[0]):
		suma_air[:,:] = suma_air[:,:] + air[i,:,:]
	media = suma_air[:,:]/(m[0])
	return media
