from netCDF4 import Dataset
from function import filename3
import datetime as dt  
from plot import plot_own,plot_media,plot_anom
from data_own import data_day
import matplotlib.pyplot as plt
from variablesfunction import ncdump
import numpy as np
from anomalia import anom2
import matplotlib.pyplot as plt
from buscafunction import buscadata

'''
Plot temporal de uma região especifica (retangulo de lats e lons)
Para realizar nosso plot temporal, possuímos dois caminhos: 
1: Plotar coordenadas específicas para todo intervalo de tempo;
2: Delimitar um retângulo com coordenadas específicas e calcular a média das latitudes e longitudes que compõe esse intervalo.
Vale ressaltar que o segundo meio é o mais seguro, haja visto que iremos trabalhar com a media das latitudes e a media das longitudes dentro do retangulo que foi criado.


'''

arquivo = 'sst.mean.anom.nc'
nc_f = arquivo
lats, lons, time, sst = filename3(nc_f)


#Adaptando o vetor dt_time para o arquivo com variável DIÁRIA
dt_time = [dt.date(1800, 1, 1) + dt.timedelta(hours=t*24)
           for t in time]


'''
Escolhendo nossa data a ser plotada. Vale lembrar que estamos falando do valor já calculado da anomalia. Logo, escolher a data 
2022,5,1 significa que estamos plotando a anomalia relacionada ao mes de Maio de 2022. 
index2 = anomalia de maio de 2022 = média de maio 2022 - média de todos os Maios de 1856 até 2022
'''

#Plotando a anomalia de Maio de 2022
data1 = dt.date(2022,5,1)
index2 = data_day(data1,dt_time)
#fig1 = plot_anom(sst[index2,:,:],lats,lons,'','')


#Com o objetivo de plotarmos o gráfico temporal para a Região de El Nino 3,4, devemos primeiramente especificar as latitudes e longitudes do retângulo que vamos criar, assim como fizemos com o plot temporal da Região do Sistema Cantareira.

#Coordenadas: Niño 3.4 (5N-5S, 170W-120W)
#Para nosso arquivo: Lons [0-360]
#Portanto nossas coordenadas se transformarão: 150 -> 30  120-> 60

nino1 = {'name': 'El Niño 3.4', 'lat': -5, 'lon': 30}
nino2 = {'name': 'El Niño 3.4', 'lat': 5, 'lon': 60}
ninoespecifico = {'name': 'El Niño 3.4', 'lat': -23.22, 'lon': 46.41}


#Criando o retangulo pde acordo com as latitudes e longitudes espcificas:
lat1 = np.abs(lats - nino1['lat']).argmin()
lon1 = np.abs(lons - nino1['lon']).argmin()
lat2 = np.abs(lats - nino2['lat']).argmin()
lon2 = np.abs(lons - nino2['lon']).argmin()


#E a coordenada específica de Nino 3,4 é:
latespecifico =  np.abs(lats - ninoespecifico['lat']).argmin()
lonespecifico = np.abs(lons - ninoespecifico['lon']).argmin()
#print(latespecifico,lonespecifico)

#Antes de calcular a média, temos um cubo como todos formado por: time, lats, lons

#Calculando a media do intervalo de latitude especifico que foi determinado acima:
medialat = np.mean(sst[:,lat1:lat2,:],axis=1)
#Após calcular a média acima, deixamos de ter um cubo por tres dimensões e passamos a ter um retangulo bidimensional: lons e time (lats deixou de ser o intervalo de lat1 - lat2 e passou a ser um ponto (média))

#A partir da media criada para latitude, calcula-se uma nova média. Como temos duas dimensões (lons e time), ao calcular a média para lons, teremos como resultado varios pontos em time representando a media de lat e lons.
#Media cubo -> Media quadrado -> Linha

mediaquadrado = np.mean(medialat[:,lon1:lon2],axis = 1)

#Dessa forma, a variável mediaquadrado é composta pelas médias das latitudes e longitutes especificas no intervalo de tempo de 1856 a 2022.

#plt.plot(dt_time[::12],mediaquadrado[::12])
#print(dt_time[::12])

#O plot abaixo é feito para a latitude espeficifica, assim como foi dito como possível no tópico 1 (as latitudes e longitudes seguem as coordenadas determinadas em latespecifico1 e latespecifico2)

#plt.plot(dt_time[::12],sst[::12,latespecifico,lonespecifico])


#Para plotar nosso gráfico referente a anomalia, precisamos dos seguintes parâmetros:
#plt.plot(coordenada horizontal, coordenada vertical, cor da linha, tipo de marcador,titulo)

#Coordenada Horizontal: Origem do vetor
#Coordenada Vertical: Anomalia

#Anomalia de todo intervalo de tempo= Vetor completo com todos os dados - media do vetor com todos os dados

#Vale lembrar que já possuímos nossa anomalia calculada. Logo, não será necessário criar um loop para que seja necessário o cálculo.

#Necesidade de criar um vetor composto somente pelo mes de Janeiro
#Necesidade de criar um vetor composto somente pelo mes de Fevereiro
#Necesidade de criar um vetor composto somente pelo mes de Março

#Vetor all time: Vetor composto pelo primeiro dia de cada ano no intervalo de 1857 até 2022. Será utilizado para determinar o tamanho do vetor que será criado com todos os meses que compõe o vetor alltime. Não será necessario criar um vetor para cada mes, pois todos irão possuir o mesmo tamanho e a mesma informação (ano).
alltime = []
firstmonth = -12

#Vetor dt_time: Composto por datas
for i in range(0,int(len(dt_time)/12)+1):
	firstmonth = firstmonth+12
	print(dt_time[firstmonth])
	year = dt_time[firstmonth]
	alltime.append(year)
	


alljanuary = []
allfev = []
firstmonth = -12
secondmonth = -11


for i in range(0,int(len(sst)/12)+1):
	soma_month = mediaquadrado[firstmonth]
	soma_month2 = mediaquadrado[secondmonth]
	firstmonth = firstmonth+ 12
	secondmonth = secondmonth + 12
	print(dt_time[firstmonth])
	alljanuary.append(soma_month)
	allfev.append(soma_month2)

	
	
	'''
alldez = []
firstmonth = 12
for i in range((len(sst)/12)):
	soma_mes = mediaquadrado[firstmonth]
	iniciodez = iniciodez + 12
	print(dt_time[iniciodez])
	alldez.append(iniciodez)
	'''

#Criar um vetor composto pela anomalia de todos janeiros 
plt.plot(alltime, alljanuary, c='r', marker = '', label ='Janeiro')
plt.plot(alltime, allfev, c='b', marker = '', label ='Fevereiro')
#plt.plot(alltime, alldez, c='g', marker = '', label ='Dezembro')

plt.ylabel('SST mm/day')
plt.xlabel('Time')
plt.title('Sea Surface Temperature from Nino 3,4')
plt.legend(loc="upper left", fontsize = 8)
plt.savefig('sst',dpi =1000, bbox_inches='tight')
plt.axhline(0.0,color = 'black', linestyle = '--')

plt.savefig('nino3_4')
plt.show()









