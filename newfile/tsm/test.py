#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import os, sys
from netCDF4 import Dataset
from function import filename3
import numpy as np
import datetime as dt  
from plot import plot_anom
from data_own import data_day
from variablesfunction import ncdump
import matplotlib.pyplot as plt
from anomalia import anom2

#Arquivo que faz as medias de maneira manual
arquivo = 'sst.mean.anom.nc'
nc_f = arquivo

#To show the file variables
nc_fid = Dataset(arquivo, 'r')  # Dataset is the class behavior to open the file
                             # and create an instance of the ncCDF4 class
#nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)
lats, lons, time, sst = filename3(nc_f)

#necessidade de arrumar dt_time
dt_time = [dt.date(1800, 1, 1) + dt.timedelta(hours=t*24)
           for t in time]


#representacao mensal (media de 3 meses)
#print(dt_time)

#2020 dates
date1= dt.date(2020,1,1)
date2 = dt.date(2020,2,1)
date3 = dt.date(2020,3,1)
index1 = data_day(date1,dt_time)
index2 = data_day(date2,dt_time)
index3 = data_day(date3,dt_time)
 	
#2021 dates 
date1= dt.date(2020,1,1)
date2 = dt.date(2020,2,1)
date3 = dt.date(2020,3,1)
index1 = data_day(date1,dt_time)
index2 = data_day(date2,dt_time)
index3 = data_day(date3,dt_time)
date4= dt.date(2021,1,1)
date5 = dt.date(2021,2,1)
date6 = dt.date(2021,3,1)
index4 = data_day(date4,dt_time)
index5 = data_day(date5,dt_time)
index6 = data_day(date6,dt_time)
 	

nino1 = {'name': 'El Niño 3.4', 'lat': 5, 'lon': 30}
nino2 = {'name': 'El Niño 3.4', 'lat': -5, 'lon': 60}
ninoespecifico = {'name': 'El Niño 3.4', 'lat': -23.22, 'lon': 46.41}

#print(lons)
#print(lats)
lat_idx1 = np.abs(lats - nino1['lat']).argmin()
lon_idx1 = np.abs(lons - nino1['lon']).argmin()
lat_idx2 = np.abs(lats - nino2['lat']).argmin()
lon_idx2 = np.abs(lons - nino2['lon']).argmin()
latespecifico = np.abs(lats - ninoespecifico['lat']).argmin()
lonespecifico = np.abs(lons - ninoespecifico['lon']).argmin()

#Dessa forma, as coordenadas do nosso retângulo são:
#print(lat_idx1,lat_idx2)
#print(lon_idx1,lon_idx2)
#print(latespecifico,lonespecifico)

medialat = np.mean(sst[:,lat_idx1:lat_idx2,:],axis = 1)
#print(lat_idx1,lat_idx2)
print(sst[:,lat_idx1:lat_idx2,:])
mediaquadrado = np.mean(medialat[:,lon_idx1:lon_idx2],axis = 1)
#print(medialat)
exit()
alljanuary = []
allfebruary = []
allmarch = []
alltime = []
alltrimestre = []
firstmonth = 0
secondmonth =1
thirdmonth = 2
#print(dt_time)
for i in range(0,int(len(dt_time)/12)):
	soma_month = mediaquadrado[firstmonth]
	soma_month2 = mediaquadrado[secondmonth]
	soma_month3 = mediaquadrado[thirdmonth]
	trime =  (mediaquadrado[firstmonth]+mediaquadrado[secondmonth]+mediaquadrado[thirdmonth])/3.0
	firstmonth = firstmonth+12
	secondmonth = secondmonth+12
	thirdmonth = thirdmonth+12
	year = dt_time[firstmonth]
	alljanuary.append(soma_month)
	allfebruary.append(soma_month2)
	allmarch.append(soma_month3)
	alltime.append(year)
	alltrimestre.append(trime)
mediajaneiro = np.mean(alljanuary)
mediafev = np.mean(allfebruary)
mediamarco = np.mean(allmarch)


#print(alljanuary)
mediatotalmeses = (mediajaneiro+mediafev+mediamarco)/3.0

#Agora: plotando todos os janeiros [79:22]
#precisa fazer: média de todos os janeiros de [79:22]
#possivel resultado:mediaaaljanuary = 2.5
#mediaquadrado[::12] - mediaalljanuary

#Para plotar nosso gráfico referente a anomalia, precisamos dos seguintes parâmetros:
#plt.plot(coordenada horizontal, coordenada vertical, cor da linha, tipo de marcador,titulo)


#print(np.abs(lats - cant['lat']))
plt.plot(alltime, alljanuary-mediajaneiro, c='r', marker = '', label ='Janeiro')
#plt.plot(alltime, alljanuary-mediajaneiro, c='r', marker = '', label ='Janeiro')
#plt.plot(alltime, alljanuary, c='r', marker = '', label ='Janeiro')
plt.plot(alltime, allfebruary-mediafev, c='g', marker = '', label ='Fevereiro')
plt.plot(alltime, allmarch-mediamarco, c='b', marker = '', label = 'Março')
plt.plot(alltime, alltrimestre-mediatotalmeses, c='k', marker = '', label = 'Quaterly')
print(mediaquadrado)
#print(alltime)
#print(alljanuary-mediajaneiro)

plt.ylabel('Precipitation mm/day')
plt.xlabel('Time')
plt.title('Quartely Anomalia Precipitation from Sistema Cantareira 2020 ')
plt.legend(loc="upper left", fontsize = 8)
plt.savefig('cantprecip2020',dpi =1000, bbox_inches='tight')
plt.axhline(0.0,color = 'black', linestyle = '--')


#fig = plt.figure()

#plt.plot(alltime, alltrimestre-mediatotalmeses, c='r', marker = '', label = 'Março')

#Plot da região que foi escolhida para estudo (Sistema Cantareira)
#square = plot_tempsquare(anomalia,lats,lons,'Quartely Precipitation Average 2020 mm/day','averagetrimestral2020')

#plt.show()
