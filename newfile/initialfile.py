from netCDF4 import Dataset
import matplotlib.pyplot as plt
from function import filename,anom
import datetime as dt  
from variablesfunction import ncdump
from data_own import data_day
from plot import plot_own
import numpy as np

nc_f = '../newfile/olr.day.mean.nc'  #When progaming in INPE
#nc_f = '/media/pedroogarcez/GARCEZ/dadosoriginais/DADOSNOVOS/olr.day.mean.nc' #When progaming in Windows Notebook
#nc_f = '/Volumes/GARCEZ/dadosoriginais/DADOSNOVOS/olr.day.mean.nc' #When programing in MacOS

nc_fid = Dataset(nc_f, 'r')  # Dataset is the class behavior to open the file
                             # and create an instance of the ncCDF4 class
nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)
lats, lons, air,time = filename(nc_f)

time_idx = 237 

# List of all times in the file as datetime objects
dt_time = [dt.date(1800, 1, 1) + dt.timedelta(hours=t) 
           for t in time]
cur_time = dt_time[time_idx]
#print(dt_time[0],dt_time[len(dt_time)-1])


#Choosing the date to plot
randon_date=dt.date(2000,1,1)
randon_date2=dt.date(2000,03,31)
index1=data_day(randon_date,dt_time)
index2=data_day(randon_date2,dt_time)


#Developing the 1800 average to plot
date_start=dt.date(2000,1,1)
date_finish=dt.date(2000,12,31)
data_anomalia=dt.date(2000,7,25)
index1=data_day(date_start,dt_time)
index2=data_day(date_finish,dt_time)
anualmedia=np.mean(air[index1:index2,:,:],axis=0)
	#anomalia to one date:
#anomalia=air[index,:,:]-anualmedia[:,:]


#chamando a função anomalia para um dia 
date_start=dt.date(2000,1,1)
date_finish=dt.date(2000,12,31)

anomalia=anom(index1,index2,air,lats,lons)

#mediaanomalia=np.mean(anomalia,axis=0)
#dt_time2 = [dt.date(1800, 1, 1) + dt.timedelta(hours=t) 
           #for t in range(time[index1],time[index2])]
#print(time[index1],time[index2])


time2=time[index1:index2]
dt_time2 = [dt.date(1800, 1, 1) + dt.timedelta(hours=t) 
           for t in time2]
index3=data_day(data_anomalia,dt_time2)



plt.savefig('OLR anomalia')
#plot_own(anomalia[index3,:,:],lats,lons,'','')
#Ploting the 1800 average temperature
#plot_own(anualmedia,lats,lons,'','')
#plot_own(anomalia[200,:,:],lats,lons,'','')


#Ploting the random date
fig1=plot_own(air[index1,:,:],lats,lons,'image','')
plt.show()

fig2=plot_own_robin(air[index1,:,:],lats,lons,'image','')

plt.show()





