from netCDF4 import Dataset 
import numpy as np

def filename2(fn):
	#nc_fid = Dataset('./2020files/%s'%(fn), 'r')  # Dataset is the class behavior to open the file
	nc_fid = Dataset(fn)
	      #nc_fid = Dataset(fn, 'r')
                             # and create an instance of the ncCDF4 class
		#Print the descripsion of the data, optional. 
	#nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)

#The  above command shown that there are 4 variables
#in the nc_f = ./air.sig995.2012.nc file.  
#The data shown the air temperature of 1 year. 
# Extract data from NetCDF file
	lats = nc_fid.variables['latitude'][:]  # extract/copy the data
	lons = nc_fid.variables['longitude'][:]
	time = nc_fid.variables['time'][:]
	level  = nc_fid.variables['level'][:]  # shape is time, lat, lon as shown above
	air = nc_fid.variables['v'][:,14,:,:]


	return lats, lons, level, time, air
	
#Observações: Devido ao tamanho do arquivo, foi necessário especificar as características do plot que iremos fazer. Logo, ao invés de enviar a variável air como um vetor de 4 dimensões, especificou-se qual seria a pressão de interesse. Como dentre todos os componentes do vetor 'Level' a pressão de 200 mbar ocupa a posição 14, enviou-se as seguintes "cooerdenadas":
	#['v'][:,14,:,:] 		Lembrando que: air = v(time,level,latitude,longitude)


