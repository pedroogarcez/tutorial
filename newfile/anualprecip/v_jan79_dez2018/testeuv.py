import pandas as pd
import os, sys
from netCDF4 import Dataset
from functionV	import  filename2
from variablesfunctionV import ncdump
from functionV import filename2
import datetime as dt 
from data_ownV import data_day
from plotV import plot_own


nc_f = 'v_jan79_dez2018.nc'

#Criando o Dataset com os dados
nc_fid = Dataset(nc_f, 'r') 

#Descobrindo as variáveis presentes no arquivo
nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)

#Definindo as variáves
lats, lons, air, time = filename2(nc_f)
print(time)
#Datetime classes	(biblioteca = datetime, classe = date/timte/timedelta,etc)
#dt.time: submódulo dedicado para tempo
#dt.timedelta: dinferença entre duas datas (será utilizado devido ao tamanho do vetor 1979 - 2018)

'''
#Criando um vetor tempo que contém todas as datas presentes no arquivo
begin_date = dt.date(1900,1,1)
#end_date = 
num_days = 43261

#print(time)
dt_time = []
for x in range (0, num_days):
    dt_time.append(begin_date + dt.timedelta(days = x))
print(dt_time)
'''
# O range (t in time) faz com que o seja criado uma variação de tempo que percorre todo o intervalo criado pela classe timedelta
	
dt_time = [dt.date(1900, 1, 1), dt.date(1901,1,1)]
#           for t in time]
print(dt_time)
date1 = dt.date(2018,6,11)
index1 = data_day(date1,dt_time)

plot_own(air[index1],lats,lons,'2012.1.1 Global Temperature','')

