import pandas as pd
import os, sys
from netCDF4 import Dataset
from functionV	import  filename2
from variablesfunctionV import ncdump
from functionV import filename2
import datetime as dt 
from data_ownV import data_day
from plotV import plot_own
import numpy as np
import matplotlib.pyplot as plt 

nc_f = 'v_jan79_dez2018.nc'

#Criando o Dataset com os dados
nc_fid = Dataset(nc_f, 'r') 

#Descobrindo as variáveis presentes no arquivo
#nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)

#Definindo as variáves
lats, lons, level, time,air = filename2(nc_f)


#Porém, nesta arquivo a variávbel time é considerada um int. Logo, não é possível criar um vetor com mais de uma posição quando estamos trabalhando com a classe timedelta. Portanto, é necessário transformar a variável time int para time float.

#Nesse processo é necessário atenção, haja visto que não é possível transformar um vetor com mais de uma posição apenas utilizando o comando float(). A maneira como iremos tranformar será utilizando a função Numpy Vectorize.
# time = float(time) [TypeError: Only length-1 arrays can be converted to Python scalars]
#time = float(time)

#Função vectorize
vector = np.vectorize(np.float)
x = np.array(time)
x = vector(x)
#print(x)

#Datetime classes	(biblioteca = datetime, classe = date/timte/timedelta,etc)
#dt.time: submódulo dedicado para tempo
#dt.timedelta: dinferença entre duas datas (será utilizado devido ao tamanho do vetor 1979 - 2018)
# O range (t in time) faz com que o seja criado uma variação de tempo que percorre todo o intervalo criado pela classe timedelta
	
dt_time = [dt.date(1900, 1, 1) + dt.timedelta(hours=t)
           for t in x]
#print(dt_time)

randon_date=dt.date(2000,1,1)
index1=data_day(randon_date,dt_time)


#Função que busca a pressão específica no vetor level
def buscapressao(lista,pressao):
	for i in range(len(level)):
		if level[i] == pressao:
			return i
	return none

#Especificando a pressão:
pressaoespecifica = 200
indicepressao = buscapressao(level,pressaoespecifica)

if indicepressao is not None:
	print('A posição do elemento {} é {}'.format(pressaoespecifica,indicepressao))
else:
	print('O elemento {} não se encontra na lista dt_time'.format(pressaoespecifica))

#corrigir essa plotagem atual
#média de todos Janeiros
#plotar a média de Janeiro
#anomalia para janeiro de 2018
#baixar arquivo com dados de 1979 ate 2022 
#anomalia de todos janeiros com referencia de 2021

#Observação: Após plotar um dia específico, agora vamos começar a trabalhar com as méidas e anomalias para a componente V do vento.
#Para cálculo da anomalia:
#Anomalia = (média de trimestre específico) - (média de todos trimestres) 
	#Como neste arquivos já possuímos a média de cada mês, primeiramente precisamos calcular a média do primeiro trimestre de 2018. Vale ressaltar que devemos 	calcular a média relacionada a air = v(index,level,lats,lons).Logo, será calculada a média de acordo com cada variável que compõe "air". Posteriormente, 		vamos criar um vetor composto pela média de todos os trimestres no intervalo de 1979 a 2018. O passo final é "subtrair esses valores".

#Criando um vetor composto pelas médias trimestrais do primeiro trimestre de 2018

data1 = dt.date(2018,1,1)
data2 = dt.date(2018,2,1)
data3 = dt.date(2018,3,1)
index2 = data_day(data1,dt_time)
index3 = data_day(data2,dt_time)
index4 = data_day(data3,dt_time)
media2018 = (air[index2,:,:]+air[index3,:,:]+air[index4,:,])/3.0
print(media2018)

#Plotando a média de 2018
fig2 = plot_own(media2018,lats,lons,'media trimestral 2018','media2018')
#anomalia = media2018 - mediatodosanos
	



#Plotando o dia 2000-1-1
fig1=plot_own(air[index1,:,:],lats,lons,'V component of wind','V')
plt.show()

