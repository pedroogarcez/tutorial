# criando um vetor tempo, com as datas presentes no arquivo v_jan79_dez2018.nc
#Observações:
'''
    Sabendo que o arquivo está referenciado a partir de 'hours since 1900-01-01 00:00:00.0' será  necessário criar 
    um vetor de tempo com as datas presentes na variável 'time'. Dessa forma, como o arquivo possui dados no in-
    tervalo de tempo desde 1979-1-1 até 2018-6-11, iremos selecionar um posição desse vetor para plotagem.
'''
import datetime as dt
data_begin = dt.date(1979,1,1)
data_finish = dt.date(2018,6,11)
difference = (data_finish - data_begin)
#print(difference) 

#difference = 14406 days

#Esse será nosso intervalo de tempo (14406 dias)
diffnumdays = 14407
dt_time = []
for i in range (0, diffnumdays):
    dt_time.append(data_finish - dt.timedelta(days = i))
print (dt_time)

#Agora, com nosso vetor dt_time criado, vamos escolher uma posição específica para plotar.

def busca(lista,data):
    for i in range(len(dt_time)):
        if dt_time[i] == data:
            return i
    return None
        
#Escolhendo uma data específica:
data = dt.date(2018,6,11)  
indice = busca(dt_time,data)
        
if indice is not None:
	print('A posição do elemento {} é {}'.format(data,indice))
else:
	print('O elemento {} não se encontra na lista dt_time'.format(data))
	
