import pandas as pd
import os, sys
from netCDF4 import Dataset
from functionV	import  filename2
from variablesfunctionV import ncdump
from functionV import filename2
import datetime as dt 
from data_ownV import data_day
from plotV import plot_own
import numpy as np


nc_f = 'v_jan79_dez2018.nc'

#Criando o Dataset com os dados
nc_fid = Dataset(nc_f, 'r') 

#Descobrindo as variáveis presentes no arquivo
nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)

#Definindo as variáves
lats, lons, level, time,air = filename2(nc_f)



#Porém, nesta arquivo a variávbel time é considerada um int. Logo, não é possível criar um vetor com mais de uma posição quando estamos trabalhando com a classe timedelta. Portanto, é necessário transformar a variável time int para time float.

#Nesse processo é necessário atenção, haja visto que não é possível transformar um vetor com mais de uma posição apenas utilizando o comando float(). A maneira como iremos tranformar será utilizando a função Numpy Vectorize.
# time = float(time) [TypeError: Only length-1 arrays can be converted to Python scalars]
#time = float(time)

#Função vectorize
vector = np.vectorize(np.float)
x = np.array(time)
x = vector(x)
print(x)

#Datetime classes	(biblioteca = datetime, classe = date/timte/timedelta,etc)
#dt.time: submódulo dedicado para tempo
#dt.timedelta: dinferença entre duas datas (será utilizado devido ao tamanho do vetor 1979 - 2018)
# O range (t in time) faz com que o seja criado uma variação de tempo que percorre todo o intervalo criado pela classe timedelta
	
dt_time = [dt.date(1900, 1, 1) + dt.timedelta(hours=t)
           for t in x]
#print(dt_time)

randon_date=dt.date(2000,1,1)
index1=data_day(randon_date,dt_time)

'''
data_begin = dt.date(1979,1,1)
data_finish = dt.date(2018,6,11)
difference = (data_finish - data_begin)
#print(difference) 

#difference = 14406 days

#Esse será nosso intervalo de tempo (14406 dias)
diffnumdays = 14407
dt_time = []
for i in range (0, diffnumdays):
    dt_time.append(data_finish - dt.timedelta(days = i))
print (dt_time)

#Agora, com nosso vetor dt_time criado, vamos escolher uma posição específica para plotar.

def busca(lista,data):
    for i in range(len(dt_time)):
        if dt_time[i] == data:
            return i
    return None
        
#Escolhendo uma data específica:
data = dt.date(2018,6,11)  
index1 = data_day(data,dt_time)
indice = busca(dt_time,data)
        
if indice is not None:
	print('A posição do elemento {} é {}'.format(data,indice))
else:
	print('O elemento {} não se encontra na lista dt_time'.format(data))


plot_own(air[indice],lats,lons,'2012.1.1 Global Temperature','')
'''

#Função que busca a pressão específica no vetor level
def buscapressao(lista,pressao):
	for i in range(len(level)):
		if level[i] == pressao:
			return i+1
	return none

#Especificando a pressão:
pressaoespecifica = 200
indicepressao = buscapressao(level,pressaoespecifica)

if indicepressao is not None:
	print('A posição do elemento {} é {}'.format(pressaoespecifica,indicepressao))
else:
	print('O elemento {} não se encontra na lista dt_time'.format(pressaoespecifica))

#corrigir essa plotagem atual
#média de todos Janeiros
#plotar a média de Janeiro
#anomalia para janeiro de 2018
#baixar arquivo com dados de 1979 ate 2022 
#anomalia de todos janeiros com referencia de 2021



fig2=plot_own(air[index1,indicepressao,:,:],lats,lons,'image','')


