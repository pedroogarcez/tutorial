from netCDF4 import Dataset
from functionadaptor import filename2
import datetime as dt  
from plotadaptor import plot_own
from data_ownadaptor import data_day
import matplotlib.pyplot as plt
from variablesfunction import ncdump
import numpy as np


import matplotlib.pyplot as plt

#Arquivo que faz as medias de maneira manual
arquivo = 'adaptor_79_22_trimestral.nc'
nc_f = arquivo

#nc_attrs, nc_dims, nc_vars = ncdump(nc_f)

lats, lons, time, velocity = filename2(nc_f)



#Função vectorize
vector = np.vectorize(np.float)
x = np.array(time)
x = vector(x)

#necessidade de arrumar dt_time
dt_time = [dt.date(1900, 1, 1) + dt.timedelta(hours=t)
           for t in x]

#print(dt_time)
#representacao mensal (media de 3 meses)


data = dt.date(2022,1,6)
index = data_day(data,dt_time)

#Função que busca a pressão específica no vetor level
def buscapressao(lista,pressao):
	for i in range(len(level)):
		if level[i] == pressao:
			return i
	
#fig = plot_own(velocity[index,:,:],lats,lons,'V component of wind m/s (2022.4.6)')
#plt.savefig('V', dpi=1000)
#plt.show()
