import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from mpl_toolkits.basemap import Basemap


fig = plt.figure(figsize=(10, 8))


#Criando a projeção 
ax = fig.add_subplot(111, projection=ccrs.PlateCarree())
#Adicionando os continentais
ax.add_feature(cfeature.LAND)

#Adicionando os oceanos 
#ax.add_feature(cfeature.OCEAN)
#Adicionando os limites dos continentes
ax.add_feature(cfeature.COASTLINE)
#Adicionando os limites de cada País
ax.add_feature(cfeature.BORDERS)
#Adicionando os rios
ax.add_feature(cfeature.RIVERS)
#Limitando o plot ao Brasil Parâmetros ([Lon Oeste, Long Leste, Lat Norte, Lat Sul])
ax.set_extent([-90,-30,10,-40],ccrs.PlateCarree())
#ax.set_extent([-180,180,-90,90],ccrs.PlateCarree())
#Adicionando os limites estaduais, com o banco de dados do Natural Earth
#Adicionando linhas de grades de latitude e longitude
g1 = ax.gridlines(crs=ccrs.PlateCarree(),draw_labels=True,color = 'black',linestyle=':')
g1.ylabels_right = False
g1.xlabels_top = False
#Cores
ax.stock_img()
#Desenhando o retângulo relacionado ao Sistema Cantareira
rectangle = plt.Rectangle((-40,-25), -15, 5.5, fc='None',ec="red")
plt.gca().add_patch(rectangle)


'''
Observação:
A região escolhida para criar o plot temporal está limitada pelas seguintes coordenadas geográficas:
cant = {'name': 'Sistema Cantareira, Brazil', 'lat': -25, 'lon': 305}
cant2 = {'name': 'Sistema Cantareira, Brazil', 'lat': -19.5, 'lon': 320}

Adaptando para a projeção de Robinson	
'''

states = cfeature.NaturalEarthFeature(category='cultural',
                                      name='admin_1_states_provinces_lines',
                                      scale='50m',
                                      facecolor='none')
                                      
       

ax.add_feature(states, edgecolor='gray', linestyle='--', linewidth=1)

plt.title('Região do Sistema Cantareira - SP',size = 15)
plt.savefig('maparegiaocant')

plt.show()
