#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import os, sys
from netCDF4 import Dataset
from function import filename3
import numpy as np
import datetime as dt  
from plot import plot_precip, plot_anom, plot_tempsquare
from data_own import data_day
import matplotlib.pyplot as plt
from anomalia import anom2
from loopfunction import loopmedia,loopprecip
from localfunction import local

def precipanom():
#Arquivo que faz as medias de maneira manual
	arquivo = '../precip/precip.mon.mean.nc'
	nc_f = arquivo

#To show the file variables
	nc_fid = Dataset(arquivo, 'r')  # Dataset is the class behavior to open the file
                             # and create an instance of the ncCDF4 class
#nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)
	lats, lons, precip,time = filename3(nc_f)

#necessidade de arrumar dt_time
	dt_time = [dt.date(1800, 1, 1) + dt.timedelta(hours=t*24) 
        	   for t in time]

#PLOT TEMPORAL
#Chamando a função responsável por calcular a média do quadrado que defininmos
	alltime, alltrimestre, mediatotalmeses = loopmedia(dt_time,precip)

#Definindo as coordenadas de Cantareira
	cant = {'name': 'Sistema Cantareira, Brazil', 'lat': -25, 'lon': 305}
	cant2 = {'name': 'Sistema Cantareira, Brazil', 'lat': -19.5, 'lon': 320}
	cantespecifico = {'name': 'Sistema Cantareira, Brazil', 'lat': -23.19, 'lon': 314}

#cant = {'name': 'Sistema Cantareira, Brazil', 'lat1': -25, 'lat2': -19.5, 'lon1': 305,'lon2':314}

#lat_idxc1,lat_idxc2,lon_idxc1,lon_idxc2 = local(cp,lats,lons)

#obs: Funções utilizadas: 
#np.abs: valor absoluto: módulo: a partir do vetor lats, cria um vetor chamado lat_idx1 composto somente por valores > 0. O mesmo se aplica para o vetor longitude
#np.argmin(): Determina a posição (index) do valor que foi especificado nas variáveis cant e cant2. Logo, após criar o vetor com módulo no através da função np.abs, a função argmin procura e retorna a posição do valor atribuído em cant e can2. Vale ressaltar que a posição começa a ser contada a partir de 0 (0,1,2,3...)

#Determinando a posição de nossas latitudes específicas em nosso vetor de latitudes (lats)
	lat_idx1 = np.abs(lats - cant['lat']).argmin()
	lat_idx2 = np.abs(lats - cant2['lat']).argmin()

#Determinando a posição de nossas longitudes específicas em nosso vetor de longitudes (lons)
	lon_idx1 = np.abs(lons - cant['lon']).argmin()
	lon_idx2 = np.abs(lons - cant2['lon']).argmin()

#Calculando a media de nosso cubo (lats,lons,time)
	medialat = np.mean(precip[:,lat_idx1:lat_idx2,:],axis = 1)
#Calculando a média do retangulo (lons,time)
	mediaquadrado = np.mean(medialat[:,lon_idx1:lon_idx2],axis = 1)

	alltimeprecip, alltrimestreprecip, mediatotalmesesprecip = loopmedia(dt_time,mediaquadrado)


#Para plotar nosso gráfico referente a anomalia, precisamos dos seguintes parâmetros:
#plt.plot(coordenada horizontal, coordenada vertical, cor da linha, tipo de marcador,titulo)

#Plot para cada mês 
#plt.plot(alltime, alljanuary-mediajaneiro, c='r', marker = '', label ='Janeiro')
#plt.plot(alltime, allfebruary-mediafev, c='g', marker = '', label ='Fevereiro')
#plt.plot(alltime, allmarch-mediamarco, c='b', marker = '', label = 'Março')

#Plot trimestral
	'''
	plt.plot(alltimeprecip, alltrimestreprecip-mediatotalmesesprecip, c='r', marker = '', label = 'Precipitation')
	plt.ylabel('JFM precipitation anomaly (mm/month)')
	plt.xlabel('Time')
	plt.title('JFM Rainfall anomaly in Cantareira System')
	plt.legend(loc="upper left", fontsize = 8)
	plt.savefig('cantprecip2020',dpi =1000, bbox_inches='tight')
	plt.axhline(0.0,color = 'black', linestyle = '--')
	'''
#Diminuir as longitudes; FOcar mais nas extremidades horizontais de Sâo Paulo
#Criar novas areas para regioes: Amazonia, NOrdeste, La PLata e SUdeste


#Media e anomalia trimestral 2020 
#Media e anomalia trimestral 2021

	return alltimeprecip, alltrimestreprecip, mediatotalmesesprecip



