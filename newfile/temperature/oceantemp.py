import pandas as pd
import os, sys
from netCDF4 import Dataset
from function import filename2
import numpy as np
import datetime as dt  
from plot import plot_own,plot_precip, plot_anom
from data_own import data_day
import matplotlib.pyplot as plt
from anomalia import anom2
from loopfunction import loopmedia,loopprecip
'''
Este arquivo será utilizado para plotagem temporal da anomalia relacionada a temperatura do Sistema Cantareira. Dessa forma, iremos criar um retângulo composto pelas coordenadas e calcular a média relacionada a essa área.
'''

#arquivo = '../anualprecip/precip.mon.mean.nc'
arquivo = 'temperatutefile.nc'
nc_f = arquivo


#To show the file variables
nc_fid = Dataset(arquivo, 'r')  # Dataset is the class behavior to open the file
                             # and create an instance of the ncCDF4 class

lats, lons, temperature, time = filename2(nc_f)

vector = np.vectorize(np.float)
x = np.array(time)
x = vector(x)

#necessidade de arrumar dt_time
dt_time = [dt.date(1900, 1, 1) + dt.timedelta(hours=t) 
           for t in x]
#print(dt_time)

date1 = dt.date(2020,1,1)
index1 = data_day(date1,dt_time)

fig = plot_precip(temperature[index1,:,:],lats,lons,'','')


plt.show()
