from temperaturefile import tempanom
from preciptationfile import precipanom
import matplotlib.pyplot as plt
#import seaborn as sns

alltime, alltrimestre, mediatotalmeses = tempanom()
alltimeprecip, alltrimestreprecip, mediatotalmesesprecip = precipanom()

anomaliaprecip = alltrimestreprecip-mediatotalmesesprecip


fig,ax1 = plt.subplots(figsize=(15,6))


vetortempo = alltimeprecip[1:42]
vetorprecip = alltrimestreprecip[1:42]

vetortemposelecionado = []
vetorprecipselecionado = []

index = 0
def selecionavetor(vetor,vetorselecionado):
	global index
	for i in range(len(vetor)):
		vetorselecionado.append(vetor[index])
		index = index + 10
		if vetor[index] == vetor[-1]:
			vetorselecionado.append(vetor[-1])
			break
	return 

selecionavetor(vetortempo,vetortemposelecionado)

index1 = 0
for i in range(len(vetorprecip)):
	vetorprecipselecionado.append(vetorprecip[index1])
	index1 = index1 + 10
	if vetorprecip[index1] == vetorprecip[-1]:
		vetorprecipselecionado.append(vetorprecip[-1])
		break



#Plotando a linha de precipitação
ax1.plot(vetortemposelecionado,vetorprecipselecionado,c='b',label='Precipitação')
ax1.legend(loc='upper right')
#Definindo a legenda 
ax1.set_ylabel('Média da precipitação (mm/dia)',fontsize=13)

#Como só possuímos informação de dados de população para 5 anos (1980,1990,2000,2010,2020), é preciso adaptar os vetores de precipitação, selecionando 
#a mesma quantidade de elementos			len(alltrimestreprecip) = len(populacao)


populacao = [122288383,150706446,175873720,196353492,213196304]


#Utilizando o mesmo eixo x da plotagem acima para criar a linha de popualação
ax2 = ax1.twinx()
ax2.plot(vetortemposelecionado,populacao,c='r',label='População')
#Definindo titulo da direita e tamanho do título
ax2.set_ylabel('Crescimento populacional',fontsize=13)
ax2.legend()

#Definindo o título superior da imagem
#plt.title('JFM Anomalia da Precipitação e Temperatura no Sistema Cantareira',fontsize=13,fontweight="bold")
#Definindo a legenda (Temperature)
plt.legend(loc="upper left", fontsize = 10)
#plt.axhline(0.0,color = 'black', linestyle = '--')
#Definindo o tamanho dos anos inferiores
ax1.tick_params(axis='x',labelsize=13,rotation=45)
#ax1.grid(b=True, which='major')
#ax1.grid(b=True, which='minor')

plt.savefig('popuxprecip')

plt.show()
