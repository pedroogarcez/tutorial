from temperaturefile import tempanom
from preciptationfile import precipanom
import matplotlib.pyplot as plt
#import seaborn as sns

alltime, alltrimestre, mediatotalmeses = tempanom()
alltimeprecip, alltrimestreprecip, mediatotalmesesprecip = precipanom()

fig,ax1 = plt.subplots(figsize=(13,5))
#plt.figure(figsize=(15,8))

#Plotando a linha de precipitação
ax1.plot(alltimeprecip,alltrimestreprecip-mediatotalmesesprecip,c='b',label='Precipitação')
ax1.legend()
#Definindo a legenda 
ax1.set_ylabel('JFM Anomalia da precipitação (mm/dia)',fontsize=13)

#Utilizando o mesmo eixo x da plotagem acima para criar a linha de temperatura
ax2 = ax1.twinx()
ax2.plot(alltime,alltrimestre-mediatotalmeses,c='r',label='Temperatura')
#Definindo titulo da direita e tamanho do título
ax2.set_ylabel('JFM Anomalia da temperatura',fontsize=13)
ax2.legend()

#Definindo o título superior da imagem
plt.title('JFM Anomalia da Precipitação e Temperatura no Sistema Cantareira',fontsize=13,fontweight="bold")
#Definindo a legenda (Temperature)
plt.legend(loc="upper left", fontsize = 10)
plt.axhline(0.0,color = 'black', linestyle = '--')
#Definindo o tamanho dos anos inferiores
ax1.tick_params(axis='x',labelsize=13,rotation=45)
#ax1.grid(b=True, which='major')
#ax1.grid(b=True, which='minor')

plt.savefig('tempxprecip')

plt.show()
