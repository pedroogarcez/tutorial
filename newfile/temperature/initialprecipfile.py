import pandas as pd
import os, sys
from netCDF4 import Dataset
from function import filename2
import numpy as np
import datetime as dt  
from plot import plot_own,plot_precip, plot_anom
from data_own import data_day
import matplotlib.pyplot as plt
from anomalia import anom2
from loopfunction import loopmedia,loopprecip
'''
Este arquivo será utilizado para plotagem temporal da anomalia relacionada a temperatura do Sistema Cantareira. Dessa forma, iremos criar um retângulo composto pelas coordenadas e calcular a média relacionada a essa área.
'''

#arquivo = '../anualprecip/precip.mon.mean.nc'
arquivo = 'temperatutefile.nc'
nc_f = arquivo


#To show the file variables
nc_fid = Dataset(arquivo, 'r')  # Dataset is the class behavior to open the file
                             # and create an instance of the ncCDF4 class

lats, lons, temperature, time = filename2(nc_f)

vector = np.vectorize(np.float)
x = np.array(time)
x = vector(x)

#necessidade de arrumar dt_time
dt_time = [dt.date(1900, 1, 1) + dt.timedelta(hours=t) 
           for t in x]

print(temperature) 
#exit()

          
#Definindo as coordenadas do Sistema Cantareira
cant = {'name': 'Sistema Cantareira, Brazil', 'lat': -19.5, 'lon': 305}
cant2 = {'name': 'Sistema Cantareira, Brazil', 'lat': -25, 'lon': 320}

lat_idx1 = np.abs(lats - cant['lat']).argmin()
lat_idx2 = np.abs(lats - cant2['lat']).argmin()

#Determinando a posição de nossas longitudes específicas em nosso vetor de longitudes (lons)
lon_idx1 = np.abs(lons - cant['lon']).argmin()
lon_idx2 = np.abs(lons - cant2['lon']).argmin()

#Calculando a media de nosso cubo (lats,lons,time)
medialat = np.mean(temperature[:,lat_idx1:lat_idx2,:],axis = 1)
#Calculando a média do retangulo (lons,time)
mediaquadrado = np.mean(medialat[:,lon_idx1:lon_idx2],axis = 1)

#Chamando a função responsável por criar os vetores especificos para cada período
alltime, alltrimestre, mediatotalmeses = loopmedia(dt_time,mediaquadrado)

plt.plot(alltime,alltrimestre-mediatotalmeses, c='r', marker = '', label = 'Temperaure')
plt.ylabel('JFM temperature anomaly (mm/month)')
plt.xlabel('Time')
plt.title('JFM Temperature anomaly in Cantareira System')
plt.legend(loc="upper left", fontsize = 8)
plt.savefig('cantprecip2020',dpi =1000, bbox_inches='tight')
plt.axhline(0.0,color = 'black', linestyle = '--')

plt.show()
