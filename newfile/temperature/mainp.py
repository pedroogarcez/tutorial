from temperaturefile import tempanom
from preciptationfile import precipanom
import matplotlib.pyplot as plt
import seaborn as sns

alltime, alltrimestre, mediatotalmeses = tempanom()
alltimeprecip, alltrimestreprecip, mediatotalmesesprecip = precipanom()

fig,ax1 = plt.subplots()
ax1.plot(alltimeprecip,alltrimestreprecip-mediatotalmesesprecip,c='r',label='Preciptation')
ax1.legend()
ax1.set_ylabel('JFM Preciptation anomaly mm/day')

ax2 = ax1.twinx()
ax2.plot(alltime,alltrimestre-mediatotalmeses,c='k',label='Temperature')
ax2.set_ylabel('JFM Temperature anomaly')
ax2.legend()

plt.title('JFM Temperature and Preciptation anomaly in Cantareira Systen')
plt.legend(loc="upper left", fontsize = 8)
plt.axhline(0.0,color = 'black', linestyle = '--')



plt.show()
