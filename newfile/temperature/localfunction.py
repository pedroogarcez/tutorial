

def local (cp,lats,lons):
	
	lat_idx1 = np.abs(lats - cp['lat1']).argmin()
	lat_idx2 = np.abs(lats - cp['lat2']).argmin()
	lon_idx1 = np.abs(lons - cp['lon1']).argmin()
	lon_idx2 = np.abs(lons - cp['lon2']).argmin()
	
	return lat_idx1,lat_idx2,lon_idx1,lon_idx2
	
