from netCDF4 import Dataset
from function import filenameV
import datetime as dt  
#from plot import plot_own,plot_anom,plot_precip
from data_own import data_day
import matplotlib.pyplot as plt
from variablesfunction import ncdump
import numpy as np
from anomalia import anom2
from matplotlib import pyplot as plt 
#from plot21 import plot_own2,plot_precip2,plot_anom2

def v_wind():
#Arquivo que faz as medias de maneira manual
	arquivo = 'mediaV79_22.nc'
	nc_f = arquivo

#nc_attrs, nc_dims, nc_vars = ncdump(nc_f)

	lats, lons, time, velocity = filenameV(nc_f)



#print(lons)
#exit()

#Função vectorize
	vector = np.vectorize(np.float)
	x = np.array(time)
	x = vector(x)

#necessidade de arrumar dt_time
	dt_time = [dt.date(1900, 1, 1) + dt.timedelta(hours=t)
        	   for t in x]

#print(dt_time)

#representacao mensal (media de 3 meses)


	data = dt.date(2022,1,6)
	index1 = data_day(data,dt_time)

#Função que busca a pressão específica no vetor level
	def buscalongitude(lista,longitude):
		for i in range(len(lons)):
			if lons[i] == longitude:
				return i

	longitudeespecifica = 179.75
	indicepressao = buscalongitude(lons,longitudeespecifica)

	if longitudeespecifica is not None:
		print('A posição do elemento {} é {}'.format(longitudeespecifica,indicepressao))
	else:
		print('O elemento {} não se encontra na lista dt_time'.format(longitudeespecifica))
	

#Criando a média do trimestre de 2020
	data1 = dt.date(2020,1,1)
	data2 = dt.date(2020,2,1)
	data3 = dt.date(2020,3,1)
	index2 = data_day(data1,dt_time)
	index3 = data_day(data2,dt_time)
	index4 = data_day(data3,dt_time)
	numerador2020 = (velocity[index2,:,:],velocity[index3,:,:],velocity[index4,:,])
	mediatri2020 = sum(numerador2020)/len(numerador2020)

#Calculando a média do trimestre de 2021
	data4 = dt.date(2021,1,1)
	data5 = dt.date(2021,2,1)
	data6 = dt.date(2021,3,1)
	index5 = data_day(data4,dt_time)
	index6 = data_day(data5,dt_time)
	index7 = data_day(data6,dt_time)
	numerador2021 = (velocity[index5,:,:],velocity[index6,:,:],velocity[index7,:,:])
	mediatri2021 = sum(numerador2021)/len(numerador2021)
	

#Criando um vetor composto por todos primeiros trimestres de 1979 a 2022
	alljanuary = []
	allfebruary = []
	allmarch = []
	firstmonth = 0
	secondmonth =1
	thirdmonth = 2
	for i in range(0,int(len(dt_time)/12)):
		soma_month = velocity[firstmonth,:,:]
		soma_month2 = velocity[secondmonth,:,:]
		soma_month3 = velocity[thirdmonth,:,:]
		firstmonth = firstmonth+12
		secondmonth = secondmonth+12
		thirdmonth = thirdmonth+12
		alljanuary.append(soma_month)
		allfebruary.append(soma_month2)
		allmarch.append(soma_month3)

#Calculando a média de todos os trimestres
	mediajan = np.mean(alljanuary,axis = 0)
	mediafev = np.mean(allfebruary,axis = 0)
	mediamar = np.mean(allmarch,axis = 0)
	mediatriallmonths = (mediajan+mediafev+mediamar)/3.0

#Calculando a anomalia 2020
	anom2020 = anom2(mediatri2020,mediatriallmonths)
#Calculando a anomalia 2021
	anom2021 = anom2(mediatri2021,mediatriallmonths)

	return mediatriallmonths,mediajan
