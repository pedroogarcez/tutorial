from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap 
from matplotlib.cm import get_cmap
from function import filename3
from plot import plot_wind
import datetime as dt  
from data_own import data_day
from variablesfunction import ncdump
from uwind import u_wind
from vwind import v_wind


#Criando o Dataset
arquivo = 'mediaUxV79_22.nc'
nc_f = arquivo
lats, lons, time, u, v = filename3(nc_f)


#Criando o vetor speed
speed = np.sqrt(u**2+v**2)
#x,y = m(lons,lats)        
#yy= np.arange(0,y.shape[0],4)
#xx = np.arange(0,x.shape[0],4)
#points = np.meshgrid(yy,xx)

mediatriallmonthsu,mediaujan,lats,lons = u_wind()
mediatriallmonthsv,mediavjan = v_wind()

#print(lats)

#fig1 = plot_wind(lats,lons,u,v,'','')
fig2 = plot_wind(lats,lons,mediaujan,mediavjan,'Média vento Janeiro 1979-2020 ','Mediavento79_22')
fig3 = plot_wind(lats,lons,mediatriallmonthsu,mediatriallmonthsv,'Média vento trimestral 1979-2020 ','Mediaventotrim79_22')

