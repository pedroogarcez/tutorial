
import numpy  as np 
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap, addcyclic, shiftgrid
from matplotlib.colors import LinearSegmentedColormap
import cartopy.crs as ccrs
import plotly.figure_factory as ff

colorpallet = LinearSegmentedColormap.from_list('mycmap', ['yellow','red','white','blue','green'], N=256, gamma=1.0)
#cor mais forte ao redor do centro, para que haja maior contraste entre -2 e 2

def plot_wind(lats,lons,u,v,plotname,figname):

	#3)To plot the medium temperature in Autumm 2012.  ################################################################### #open figure 
    fig = plt.figure() #Adjust the location of the interior of the figgure fig.subplots_adjust(left=0., right=1., bottom=0., top=0.9) # Setup the map. See http://matplotlib.org/basemap/users/mapsetup.html # for other projections.  #proj = ccrs.PlateCarree() 
	#proj = 'moll'
	
    #proj  = 'cyl'
    #proj  = 'moll'
    proj  = 'cyl'
    
    #Define latitudes to plot (-90,90)
    
    lat_i =  -90 
    lat_f =   90
    
    #Define longitudes  to plot (0,360)
    lon_i =  00.0 
    lon_f =  360.0
    
    m = Basemap(projection=proj, llcrnrlat=lat_i, urcrnrlat=lat_f,\
                llcrnrlon=lon_i, urcrnrlon=lon_f, resolution='c', lon_0=0)
    
    #m = Basemap(projection='moll', llcrnrlat=-90, urcrnrlat=90,\
    #            llcrnrlon=0, urcrnrlon=360, resolution='c', lon_0=0)
    
    m.drawcoastlines()
    m.drawmapboundary()
    m.drawcountries()
    m.drawparallels(np.arange(-60.,60.,30.),labels=[1,0,0,0]) # draw parallels
    m.drawmeridians(np.arange(-180.,180.,60.),labels=[0,0,0,1]) # draw meridians
    
    
    

    lon2d, lat2d = np.meshgrid(lons, lats[:])
    # Transforms lat/lon into plotting coordinates for projection
    x, y = m(lon2d, lat2d)
    
    yy = np.arange(0,y.shape[0],20)
    xx = np.arange(0,x.shape[1],20)
    points = np.meshgrid(yy,xx)
    speed = np.sqrt(u**2+v**2)
   

   #plt.quiver(x[::4],y[::4],u[::4,::4],v[::4,::4])
    cb = plt.quiver(x[points],y[points],u[points],v[points],speed[points])
    plt.colorbar(cb)
    
    
    
    lats = lats[::-1]
    u = u[::1,::-1]
    v = v[::1,::-1]
    
    lon2d, lat2d = np.meshgrid(lons, lats[:])
    x, y = m(lon2d, lat2d)
    
    
    plt.streamplot(x,y,u,v,density = 2)
    plt.title("%s"%(plotname))
    plt.savefig('%s'%(figname), dpi=1000)
    plt.show()
    return fig
    
    
#Para plotar a componente do vento, tem-se como necessidade criar uma grade que irá conter os pontos de origem dos vetores (função np.meshgrid). Cada ponto de origem dos vetores é definido pela função np.arange tanto para as direções horizontais(xx) quanto para verticais (yy). Os parâmetros são (origem do plano, yshape[] = até o tamanho limite do vetor y, que possui posição 0, número de intervalos de cada vetor). Com esse retângulo criado, alimentamos a função quiver com cada parâmetro tendo origem nos pontos criados a partir da descrição acima. 

#É necessário espaçar nossos vetores para que não se sobreponham uns aos outros. Para isso, usamos a função np.arange
    


	       
