import matplotlib.pyplot as plt
import numpy as np
import cartopy.feature as cfeature
import cartopy.crs as ccrs

#Criando o mapa do Brasil

fig = plt.figure(figsize=(10, 8))


#Criando a projeção 
ax = fig.add_subplot(111, projection=ccrs.PlateCarree())
#Adicionando os continentais
ax.add_feature(cfeature.LAND)

#Adicionando os oceanos 
#ax.add_feature(cfeature.OCEAN)
#Adicionando os limites dos continentes
ax.add_feature(cfeature.COASTLINE)
#Adicionando os limites de cada País
ax.add_feature(cfeature.BORDERS)
#Adicionando os rios
ax.add_feature(cfeature.RIVERS)
#Limitando o plot ao Brasil Parâmetros ([Lon Oeste, Long Leste, Lat Norte, Lat Sul])
#ax.set_extent([-90,-30,10,-40],ccrs.PlateCarree())
ax.set_extent([-180,180,-90,90],ccrs.PlateCarree())
#Adicionando os limites estaduais, com o banco de dados do Natural Earth
#Adicionando linhas de grades de latitude e longitude
g1 = ax.gridlines(crs=ccrs.PlateCarree(),draw_labels=True,color = 'black',linestyle=':')
g1.ylabels_right = False
g1.xlabels_top = False
#Cores
ax.stock_img()


'''
#Nesse arquivo, vamos aprender como plotar um campo vetorial através da funçõa plt.quiver()
#Primeiramente, vamos aprender como plotar um vetor:


X = [0] 
Y = [0] 
U = [3]   
V = [2]   

plt.quiver(X, Y, U, V, color='b', units='xy', scale=1) 
plt.title('Single Vector') 
plt.xlim(-2, 5) 
plt.ylim(-2, 2.5) 
plt.grid() 
plt.show() 


A função plt.quiver utiliza os seguintes parametros:
X,Y = Origem do vetor
U,V = Direção do vetor
Logo, como estamos utilizando uma escala de 1 e as unidades xy o vetor termina nos pontos U,V
'''

#Agora, o próximo passo é criarmos um campo vetorial composto por mais de um vetor
#Para isso, iremos seguir o mesmo roteiro, porém agora precisamos determinar as coordenadas e direções de cada vetor

#Determinando a origem de cada vetor
x, y = np.meshgrid(np.linspace(-180, 180, 10),  
                   np.linspace(-90, 90, 10))

#np.meshgrid(): Função utilizada para criar uma grade retangular a partir das matrizes unidimensionais 
#np.linspace(): Parâmetros(primeiro ponto,último ponto, número de pontos uniformementes distribuídos no intervalo)

#Logo, nossa grade retangular irá variar de -10,10 em ambas as coordenadas, com 20 vetores igualmentes distribuídos em cada dimensão

#Determinando a direção de cada vetor
u = y/np.sqrt(x**2 + y**2)
v = x/np.sqrt(x**2 + y**2)

#Chamando a função quiver
plt.quiver(x,y,u,v)

plt.title('Vector plot')
plt.savefig('vectorplot')
plt.show()



