from netCDF4 import Dataset 
import numpy as np

def filename3(fn):
		#nc_fid = Dataset('./2020files/%s'%(fn), 'r')  # Dataset is the class behavior to open the file
	nc_fid = Dataset(fn, 'r')
        #nc_fid = Dataset(fn, 'r')
                             # and create an instance of the ncCDF4 class
		#Print the descripsion of the data, optional. 
	#nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)

#The  above command shown that there are 4 variables
#in the nc_f = ./air.sig995.2012.nc file.  
#The data shown the air temperature of 1 year. 
# Extract data from NetCDF file
	#['nome da variável']['posição no vetor']

	lats = nc_fid.variables['latitude'][:]  # extract/copy the data
	lons = nc_fid.variables['longitude'][:]
	time = nc_fid.variables['time'][:]
	u = nc_fid.variables['u'][0,:,:]
	v = nc_fid.variables['v'][0,:,:]
	nc_fid.close()
	return lats, lons, time, u,v
	
#Ao estudarmos as variáveis U e V, chegamos a conclusão de que são dependentes de latitude, longitue e time. Dessa forma, forma-se um cubo composto por x = longitute, y = latitude e z = time. Logo, u = nc_fide.variables['u'][0,:,:] transforma nosso cubo em um plano, haja visto que a posição 0 do vetor 'time' faz com que ele possua somente duas dimensões.

def filenameU(fn):
		#nc_fid = Dataset('./2020files/%s'%(fn), 'r')  # Dataset is the class behavior to open the file
	nc_fid = Dataset(fn, 'r')
        #nc_fid = Dataset(fn, 'r')
                             # and create an instance of the ncCDF4 class
		#Print the descripsion of the data, optional. 
	#nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)

#The  above command shown that there are 4 variables
#in the nc_f = ./air.sig995.2012.nc file.  
#The data shown the air temperature of 1 year. 
# Extract data from NetCDF file
	#['nome da variável']['posição no vetor']

	lats = nc_fid.variables['latitude'][:]  # extract/copy the data
	lons = nc_fid.variables['longitude'][:]
	time = nc_fid.variables['time'][:]
	u = nc_fid.variables['u'][:,:,:]
	nc_fid.close()
	return lats, lons, time, u
	
def filenameV(fn):
		#nc_fid = Dataset('./2020files/%s'%(fn), 'r')  # Dataset is the class behavior to open the file
	nc_fid = Dataset(fn, 'r')
        #nc_fid = Dataset(fn, 'r')
                             # and create an instance of the ncCDF4 class
		#Print the descripsion of the data, optional. 
	#nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)

#The  above command shown that there are 4 variables
#in the nc_f = ./air.sig995.2012.nc file.  
#The data shown the air temperature of 1 year. 
# Extract data from NetCDF file
	#['nome da variável']['posição no vetor']

	lats = nc_fid.variables['latitude'][:]  # extract/copy the data
	lons = nc_fid.variables['longitude'][:]
	time = nc_fid.variables['time'][:]
	v = nc_fid.variables['v'][:,:,:]
	nc_fid.close()
	return lats, lons, time, v
