from netCDF4 import Dataset
from function import filename2
import datetime as dt  
from plot import plot_own
from data_own import data_day
import matplotlib.pyplot as plt
from variablesfunction import ncdump
import numpy as np


import matplotlib.pyplot as plt


nc_f = 'Jan1979_2022.nc'

#nc_attrs, nc_dims, nc_vars = ncdump(nc_f)

lats, lons, time, velocity = filename2(nc_f)
#print(lats)
#print(lons)

#media todos janeiros 79 a 22
#criar um arquivo nc que salve a media como variável
#(lats,lonstime,media)
#####################################
#criar um arquivo com a média de 2020
#carregar o arquivo com a média de 79 a 22
#caclular anomalia

#Função vectorize
vector = np.vectorize(np.float)
x = np.array(time)
x = vector(x)

#necessidade de arrumar dt_time
dt_time = [dt.date(1900, 1, 1) + dt.timedelta(hours=t)
           for t in x]


#print(dt_time)
#representacao mensal (media de 3 meses)

data = dt.date(1990,1,6)
index = data_day(data,dt_time)
	
fig = plot_own(velocity[index,:,:],lats,lons,'V component of wind m/s (2022.4.6)')
#plt.savefig('V', dpi=1000)
plt.show()
