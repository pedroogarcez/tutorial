from netCDF4 import Dataset
import matplotlib.pyplot as plt
from function import filename
import datetime as dt  
from variablesfunction import ncdump
from data_own import data_day
from plot import plot_own

nc_f = '../newfile/olr.day.mean.nc'  # Your filename
#nc_f = '/media/pedroogarcez/GARCEZ/dadosoriginais/DADOSNOVOS/olr.day.mean.nc'

nc_fid = Dataset(nc_f, 'r')  # Dataset is the class behavior to open the file
                             # and create an instance of the ncCDF4 class
nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)
lats, lons, air,time = filename(nc_f)

time_idx = 237 
#The calendar of reanalysis is out of phase of two days respect
offset = dt.timedelta(hours=48)
# List of all times in the file as datetime objects
dt_time = [dt.date(1800, 1, 1) + dt.timedelta(hours=t) - offset\
           for t in time]

cur_time = dt_time[time_idx]

#Choosing the data to plot
randon_date=dt.date(2012,1,1)
index=data_day(randon_date,dt_time)

#Ploting
plot_own(air[index,:,:],lats,lons,'image','')



