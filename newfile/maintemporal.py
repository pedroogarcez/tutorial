'''
NAME
    NetCDF with Python
PURPOSE
    To demonstrate how to read and write data with NetCDF files using
    a NetCDF file from the NCEP/NCAR Reanalysis.
    Plotting using Matplotlib and Basemap is also shown.
PROGRAMMER(S)
    Chris Slocum
    Jhonatan M. 
REVISION HISTORY
    20140320 -- Initial version created and posted online
    20140722 -- Added basic error handling to ncdump
                Thanks to K.-Michael Aye for highlighting the issue
    20210114 -- Statistical work with the data
                Jhonatan  
REFERENCES
    netcdf4-python -- http://code.google.com/p/netcdf4-python/
    NCEP/NCAR Reanalysis -- Kalnay et al. 1996
        http://dx.doi.org/10.1175/1520-0477(1996)077<0437:TNYRP>2.0.CO;2
'''

# Python standard library datetime  module
import datetime as dt  
# http://code.google.com/p/netcdf4-python/
import numpy as np

from netCDF4 import Dataset 

import matplotlib.pyplot as plt

#from  read_ncfiles import ncdump

#To import the library necessary to make the maps 
from mpl_toolkits.basemap import Basemap, addcyclic, shiftgrid

from data_own import data_day

from plot import plot_own, plot_temporal, plot_temporal2, plot_temporal3

from function import filename,fe,local

from function2 import mean,meanp


#forma diferente de importar a função
#import function2 as io

'''
Seasons beginning
Autumm = [(2012,3,20) - (2012,6,20)]
Winter = [(2012,6,20) - (2012,9,22)]
Spring = [(2012,9,20) - (2012,12,21)]
Summer = [(2012,12,21) - (2012,12,31)]
'''



# ../original = retornando uma pasta que já contenha o arquivo 
nc_f = '../original/air.sig995.2012.nc'
nc = '../original/air.departure.sig995.2012.nc'


lats,lons,air,time=fe(nc_f)
lats2,lons2,air2,time2=filename(nc)

time_idx = 237
offset = dt.timedelta(hours=48)

dt_time = [dt.date(1, 1, 1) + dt.timedelta(hours=t) - offset\
           for t in time]
dt_time2 = [dt.date(1, 1, 1) + dt.timedelta(hours=t) - offset\
           for t in time2]


indexlat,indexlon = local(-22.39,-45,lats,lons)
indexlat_cp2,indexlon_cp2=local(-22.39,-45,lats2,lons2)

data = data_day(time_idx,dt_time)
#Plot COM CHAMADA DE FUNÇÃO
#plot_temporal(dt_time,air[:,indexlat,indexlon])
#plot_temporal(dt_time2,air2[:,indexlat_cp2,indexlon_cp2])
#chamar uma função que irá colocar o pointer no dia especificado 
plot_temporal2(dt_time[time_idx],air[time_idx,indexlat,indexlon],dt_time,air[:,indexlat,indexlon])
plot_temporal2(dt_time2[time_idx],air[time_idx,indexlat_cp2,indexlon_cp2],dt_time2,air[:,indexlat_cp2,indexlon_cp2])
#plot_temporal3(dt_time,air[:,indexlat,indexlon],time_idx)


'''
#Plot sem chamada de função
plt.plot(dt_time, air[:, indexlat, indexlon], c='r', marker = '')
plt.plot(dt_time, air[time_idx, indexlat, indexlon], c='b', marker='o')
plt.show()
'''
