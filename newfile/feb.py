from netCDF4 import Dataset
from function import filename

from variablesfunction import ncdump


nc_f = '../newfile/feb.nc'  # Your filename
nc_fid = Dataset(nc_f, 'r')  # Dataset is the class behavior to open the file
                             # and create an instance of the ncCDF4 class
nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)
