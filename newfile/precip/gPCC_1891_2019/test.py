import pandas as pd
from netCDF4 import Dataset
from function import filename2
from mpl_toolkits.basemap import Basemap
import numpy as np
import datetime as dt
from plot import plot_precip, plot_anom
from data_own import data_day
import matplotlib.pyplot as plt
from anomalia import anom2

arquivo = 'full_data_monthly_v2020_2011_2019_025.nc'
nc_f = arquivo


#To show the file variables
nc_fid = Dataset(arquivo, 'r')  # Dataset is the class behavior to open the file
                             # and create an instance of the ncCDF4 class
#nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)
lats, lons, precip, time = filename2(nc_f)

#necessidade de arrumar dt_time
dt_time = [dt.date(2011, 1, 1) + dt.timedelta(hours=t)
           for t in time]

#O objetivo aqui é calcular a anomalia referente aos meses de JFM de 2019
# Anomaliaprecip = médiaprecip(JFM2019) - médiaprecip(JFM(2011-2019))

#Médiaprecip(JGM2019) = (jprecip2019 + fprecip2019 + mprecip2019)/3.0

jan2019 = dt.date(2019,1,1)
fev2019 = dt.date(2019,2,1)
marco2019 = dt.date(2019,3,1)
index1 = data_day(jan2019,dt_time)
index2 = data_day(fev2019,dt_time)
index3 = data_day(marco2019,dt_time)

#Selecionando os meses de janeiro, fevereiro e março de 2019
janeiro2019 = precip[index1,:,:]
fevereiro2019 = precip[index2,:,:]
marco2019 = precip[index3,:,:]


#Calculando médiaprecip(JFM2019)
mediaprecipJFM2019 = (janeiro2019+fevereiro2019+marco2019)/3.0
print(len(mediaprecipJFM2019))
exit()

#Agora calculando a média de precipitação para o intervalode de 2011 a 2019
#Definindo as coordenadas do Sistema Cantareira
cant = {'name': 'Sistema Cantareira, Brazil', 'lat': -19.5, 'lon': 305}
cant2 = {'name': 'Sistema Cantareira, Brazil', 'lat': -25, 'lon': 320}

lat_idx1 = np.abs(lats - cant['lat']).argmin()
lat_idx2 = np.abs(lats - cant2['lat']).argmin()

# Determinando a posição de nossas longitudes específicas em nosso vetor de longitudes (lons)
lon_idx1 = np.abs(lons - cant['lon']).argmin()
lon_idx2 = np.abs(lons - cant2['lon']).argmin()

# Calculando a media de nosso cubo (lats,lons,time)
medialat = np.mean(precip[:, lat_idx1:lat_idx2, :], axis=1)

#Calculando a média do retangulo (lons,time)
mediaquadrado = np.mean(medialat[:,lon_idx1:lon_idx2],axis = 1)
print(mediaquadrado)


#Agora selecionando os meses de JFM para a coordenada do Sistema Cantareira
janeiro = np.mean(mediaquadrado[::12])
fevereiro = np.mean(mediaquadrado[1:108:12])
marco = np.mean(mediaquadrado[2:48:12])

mediaJFM2011_2019 = (janeiro+fevereiro+marco)/3.0

#Calculando a anomalia
anomalia2019 = mediaprecipJFM2019 - mediaJFM2011_2019

print(len(anomalia2019))


