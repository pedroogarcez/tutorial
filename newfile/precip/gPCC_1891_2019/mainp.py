from ano1891_1900 import media1891_1900
from ano1901_1910 import media1901_1910
from ano1911_1920 import media1911_1920
from ano21_30 import media1921_1930
from ano31_40 import media1931_1940
from ano41_50 import media1941_1950
from ano51_60 import media1951_1960
from ano61_70 import media1961_1970
from ano71_80 import media1971_1980
from ano81_90 import media1981_1990
from ano91_00 import media1991_2000
from ano2001_2010 import media2001_2010
from ano2011_2019 import anomalia2011_2019
import matplotlib.pyplot as plt
from plot import plot_precip, plot_anom
import numpy as np

''' 
Aqui importamos o valor referente a média JFM de cada década no intervalo 1890 a 2019. 
Por último, temos a média de JFM de 2019 para calulo da anomalia.
'''
media1891_1900JFM,time1891_1900 = media1891_1900()
media1901_1910JFM,time1901_1910 = media1901_1910()
media1911_1920JFM,time1911_1920 = media1911_1920()
media1921_1930JFM,time1921_1930 = media1921_1930()
media1931_1940JFM,time1931_1940 = media1931_1940()
media1941_1950JFM,time1941_1950 = media1941_1950()
media1951_1960JFM,time1951_1960 = media1951_1960()
media1961_1970JFM,time1961_1970 = media1961_1970()
media1971_1980JFM,time1971_1980 = media1971_1980()
media1981_1990JFM,time1981_1990 = media1981_1990()
media1991_2000JFM,time1991_2000 = media1991_2000()
media2001_2010JFM,time2011_2010 = media2001_2010()
media2019JFM, media2011_2019JFM, lats, lons,time2011_2019,precip = anomalia2011_2019()

print(lats)
exit() 
#Calculando a média no intervalo de 18991 a 2019
media1891_2019 = (media1891_1900JFM + media1901_1910JFM + media1911_1920JFM + media1921_1930JFM + media1931_1940JFM + media1941_1950JFM \
+ media1951_1960JFM + media1961_1970JFM + media1971_1980JFM + media1981_1990JFM + media1991_2000JFM + media2001_2010JFM + media2011_2019JFM)/13.0


#Calculando a anomalia entre o ano de 2019 JFM e o intervalo de 1891 até 2019
anomalia2019 = media2019JFM - media1891_2019
#print(len(anomalia2019))


#Plotando a anomalia 
#fig11 = plot_anom(anomalia2019,lats,lons,'anomalia1891_2019JFM','anom1891_2019JF')


#Vamos agora, começar a trabalhar para plotar o intervalo de tempo (1891-2019)
#print(len(anomalia2019))

#Eixo x: Anos: 1891-2019
dt_time = (time1891_1900 + time1901_1910 + time1911_1920 + time1921_1930 + time1931_1940 + time1941_1950 + time1951_1960 + time1961_1970 + time1971_1980 \
 + time1981_1990 + time1991_2000 + time2011_2010 + time2011_2019)


#Limitando o dt_time para somente os meses de JFM
vetorjaneiro = dt_time[0:len(dt_time):12]
vetorfevereiro = dt_time[1:1538:12]
vetormarco = dt_time[2:1539:12]

jan2019 = dt.date(2019,1,1)
index1 = data_day(jan2019,dt_time)
#Eixo x
eixox = dt_time[::12]



'''
timeselect = []

for i in range(len(dt_timejaneiro)):
	timeselect.append(dt_timejaneiro[i])
	if dt_timejaneiro[i] == dt_timejaneiro[-1]:
		timeselect.append(dt_timejaneiro[-1])
		break
print(timeselect)
'''



#Eixo y: anomalia de precipitação 1891-2019
#print(len(anomalia2019))

'''
def busca_tsm(arquivo):
	for i in range(len(lista_arquivos)):
	lista_tsm.append(tsm[i])
	return
'''
