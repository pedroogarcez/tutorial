import pandas as pd
import os, sys
from netCDF4 import Dataset
from function import filename2
import numpy as np
import datetime as dt  
from plot import plot_precip, plot_anom
from data_own import data_day
import matplotlib.pyplot as plt
from anomalia import anom2

def anomalia2011_2019():

#Arquivo que faz as medias de maneira manual
	arquivo = 'full_data_monthly_v2020_2011_2019_025.nc'
	nc_f = arquivo


#To show the file variables
	nc_fid = Dataset(arquivo, 'r')  # Dataset is the class behavior to open the file
                             # and create an instance of the ncCDF4 class

	lats, lons, precip, time = filename2(nc_f)

#Criando o vetor dt_time que contém as datas contidas no arquivo
	dt_time = [dt.date(2011, 1, 1) + dt.timedelta(hours=t) 
        	   for t in time]



#Selecionando as datas para calcular a média do primeiro trimestre de 2019
	jan2019 = dt.date(2019,1,1)
	fev2019 = dt.date(2019,2,1)
	marco2019 = dt.date(2019,3,1)
	index1 = data_day(jan2019,dt_time)
	index2 = data_day(fev2019,dt_time)
	index3 = data_day(marco2019,dt_time)

#Criando 3 vetores compostos somente pelos dados que pertencem aos meses de Janeiro, fevereio e Março
	alljanuary = []
	allfebruary = []
	allmarch = []
	firstmonth = 0
	secondmonth =1
	thirdmonth = 2
	for i in range(0,int(len(dt_time)/12)):
		soma_month = precip[firstmonth,:,:]
		soma_month2 = precip[secondmonth,:,:]
		soma_month3 = precip[thirdmonth,:,:]
		firstmonth = firstmonth+12
		secondmonth = secondmonth+12
		thirdmonth = thirdmonth+12
		alljanuary.append(soma_month)
		allfebruary.append(soma_month2)
		allmarch.append(soma_month3)
		
#Calculando a média desses vetores:
	mediajaneiro = np.mean(alljanuary,axis = 0)
	mediafevereiro =np.mean(allfebruary,axis = 0) 
	mediamarco =np.mean(allmarch,axis = 0) 

#Calculando a média de 2019
	media2019JFM = (precip[index1,:,:] + precip[index2,:,:] + precip[index3,:,:])/3.0

#Calculando a media de 2011 a 2019
	media2011_2019JFM = (mediajaneiro+mediafevereiro+mediamarco)/3.0

#Calculando a anomalia 
	anomalia2011_2019JFM = media2019JFM - media2011_2019JFM

#Plotando a anomalia 
	#fig11 = plot_anom(anomalia2011_2019JFM,lats,lons,'anomalia2011_2019JFM','anom2011_2019JF')
	return media2019JFM,media2011_2019JFM


anomalia2011_2019()


