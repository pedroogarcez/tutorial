import pandas as pd
import os, sys
from netCDF4 import Dataset
from function import filename2
import numpy as np
import datetime as dt  
from plot import plot_precip, plot_anom
from data_own import data_day
import matplotlib.pyplot as plt
from anomalia import anom2
from loopfunction import loopmedia
from indicesiod import iod



#Arquivo que faz as medias de maneira manual
arquivo = 'gpcc_precip_1891_2019.nc'
nc_f = arquivo


#To show the file variables
nc_fid = Dataset(arquivo, 'r')  # Dataset is the class behavior to open the file
                             # and create an instance of the ncCDF4 class
#nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)
lats, lons, precip, time = filename2(nc_f)
precip = precip/30.0
#print(precip[0,9,0:10])
#print(np.ma.mean(precip[0,9,0:10]))
#print(lats)
#print(lons)




#precip=list(np.array(precip).astype(np.float32))
#precip =list(np.array(precip).astype(np.float32))


#necessidade de arrumar dt_time
dt_time = [dt.date(1891, 1, 1) + dt.timedelta(hours=t) 
           for t in time]


#Objetivo: Calcular a anomalia referente a JFM de 2019
#AnomaliaJFM2019 = médiaJFM2019 - mediaJFM1891-2019

#1) Vamos calcular a média de 2019
#Selecioando os meses de JFM

date1= dt.date(2018,12,1)
date2 = dt.date(2019,2,1)
date3 = dt.date(2019,3,1)
index1 = data_day(date1,dt_time)
index2 = data_day(date2,dt_time)
index3 = data_day(date3,dt_time)

#Calculando a média do trimestre de 2019
media2019 = (precip[index1,:,:]+precip[index2,:,:]+precip[index3,:,:])/3.0

#Selecionando JFM no intervalo de 1891 até 2019
dez1891_2019 = precip[12:len(precip):12]
fev1891_2019 = precip[1:len(precip):12]
marco1891_2019 = precip[2:len(precip):12]


#Calculando a média referente a cada mes no intervalo 1891_2019
mediadez1891_2019 = np.mean(dez1891_2019,axis=0)
mediafev1891_2019 = np.mean(fev1891_2019,axis=0)
mediamarco1891_2019 = np.mean(marco1891_2019,axis=0)

#Calculando a média do trimestre 1891_2019
mediatrimestral1891_2019 = (mediadez1891_2019 + mediafev1891_2019 + mediamarco1891_2019)/3.0

#Calculando a anomalia trimestral JFM 1891_2019
anomJFM1891_2019 = media2019-mediatrimestral1891_2019
#Plotando o gráfico da anomalia da américa do sul
#fig = plot_anom(anomJFM1891_2019,lats,lons,'Anomalia precipitação (mm/dia) verão (DJF) 1891_2019','anomprecip1891_2019')




# --
#Agora vamos criar o plot de linhas, referentes as coordenadas do sistema cantareira
#Objetivo: Plotar um gráfico de linhas da anomalia de DJZ no intervalo de 1891-2019
#Anomalia = médiacantareiraDJF1891-2019 - médiatodosmesesglobalDJF1891-2019 
#Definindo as coordenadas do sistema cantareira
cant = {'name': 'Sistema Cantareira, Brazil', 'lat': -19.5, 'lon': -55}
cant2 = {'name': 'Sistema Cantareira, Brazil', 'lat': -25, 'lon': -40}


#Determinando a posição de nossas latitudes específicas em nosso vetor de latitudes (lats) para Cantareira, 
lat_idx1 = np.abs(lats - cant['lat']).argmin()
lat_idx2 = np.abs(lats - cant2['lat']).argmin()
lon_idx1 = np.abs(lons - cant['lon']).argmin()
lon_idx2 = np.abs(lons - cant2['lon']).argmin()
#Calculando a média das latitude (cubo->retangulo)
medialat = np.ma.mean(precip[:,lat_idx1:lat_idx2,:],axis = 1)

#Calculnado a média da longitude
mediaquadrado = np.ma.mean(medialat[:,lon_idx1:lon_idx2],axis = 1)



jan1891_2019 = mediaquadrado[0:len(precip):12]
fev1891_2019 = mediaquadrado[1:len(precip):12]
marco1891_2019 = mediaquadrado[2:len(precip):12]
alltrimestre = (jan1891_2019 + fev1891_2019 + marco1891_2019)/3.0

mediajaneiro = np.mean(jan1891_2019)
mediafevereiro = np.mean(fev1891_2019)
mediamarco = np.mean(marco1891_2019)
mediatotalmeses = (mediajaneiro+mediafevereiro+mediamarco)/3.0


anomalia = alltrimestre-mediatotalmeses
alltime = dt_time[0:1548:12]

ax1 = plt.subplots()
ax1.plot(alltime,anomalia, c='r', marker = '', label = '')
ax2 = ax1.twinx()
ax2.plot(datas,iodindex)

'''
#Selecionando os valores de precipitação para área que selecionamos acima
jancant = mediaquadrado[0:len(dt_time):12]
fevcant = mediaquadrado[1:len(dt_time):12]
marcant = mediaquadrado[2:len(dt_time):12]


##Calculando a média referente a cada mes no intervalo 1891_2019 para o sistema cantareira
mediajancant = np.mean(jancant,axis=0)
mediafevcant = np.mean(fevcant,axis=0)
mediamarcant = np.mean(marcant,axis=0)


#Selecionando os valores de precipitação no sistema cantateira
trimestre = (jancant+fevcant+marcant)/3.0



##Calculando a média do trimestre 1891_2019 para o sistema cantareira
mediatrimestralcant = (mediajancant+mediafevcant+mediamarcant)/3.0

anomalia = trimestre - mediatrimestralcant

print(mediatrimestralcant)
exit()
#Criando o gráfico de linhas que irá plotar os valores da anomalia no eixo y e os anos no eixo x
#Selecionando os valores do eixo x
eixox = dt_time[0:len(dt_time):12]
eixoy = anomalia

plt.plot(eixox,eixoy, c='k', marker = '', label = 'Cantareira')

'''
plt.show()
