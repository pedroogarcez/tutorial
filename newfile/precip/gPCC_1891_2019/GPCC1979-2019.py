from ano1891_1900 import media1891_1900
from ano1901_1910 import media1901_1910
from ano1911_1920 import media1911_1920
from ano21_30 import media1921_1930
from ano31_40 import media1931_1940
from ano41_50 import media1941_1950
from ano51_60 import media1951_1960
from ano61_70 import media1961_1970
from ano71_80 import media1971_1980
from ano81_90 import media1981_1990
from ano91_00 import media1991_2000
from ano2001_2010 import media2001_2010
from ano2011_2019 import anomalia2011_2019
import matplotlib.pyplot as plt
from plot import plot_precip, plot_anom

''' 
Aqui importamos o valor referente a média JFM de cada década no intervalo 1890 a 2019. 
Por último, temos a média de JFM de 2019 para calulo da anomalia.
'''
mediajaneiro2011_2019
media1891_1900JFM = media1891_1900()
media1901_1910JFM = media1901_1910()
media1911_1920JFM = media1911_1920()
media1921_1930JFM = media1921_1930()
media1931_1940JFM = media1931_1940()
media1941_1950JFM = media1941_1950()
media1951_1960JFM = media1951_1960()
media1961_1970JFM = media1961_1970()
media1971_1980JFM = media1971_1980()
media1981_1990JFM = media1981_1990()
media1991_2000JFM = media1991_2000()
media2001_2010JFM = media2001_2010()
media2019JFM, media2011_2019JFM, lats, lons, mediajaneiro2011_2019 = anomalia2011_2019()


#Calculando a média no intervalo de 18991 a 2019
media1891_2019 = (media1891_1900JFM + media1901_1910JFM + media1911_1920JFM + media1921_1930JFM + media1931_1940JFM + media1941_1950JFM \
+ media1951_1960JFM + media1961_1970JFM + media1971_1980JFM + media1981_1990JFM + media1991_2000JFM + media2001_2010JFM + media2011_2019JFM)/13.0


#Calculando a anomalia entre o ano de 2019 JFM e o intervalo de 1891 até 2019
anomalia2019 = media2019JFM - media1891_2019

#Plotando a anomalia 
#fig11 = plot_anom(anomalia2019,lats,lons,'anomalia1891_2019JFM','anom1891_2019JF')

#Comparando o plot de precipitação deste arquivo com o já feito de GPCP.
media1979_2019 = (media1981_1990JFM + media1991_2000JFM + media2001_2010JFM + media2011_2019JFM)/4.0

#Calculando a anomalia referente ao intervalo de 40 anos 
anomalia1979_2019 = media2019JFM - media1979_2019

#Plotando a anomalia de 40 anos em relação a 2019
#fig11 = plot_anom(anomalia1979_2019,lats,lons,'anomalia1981_2019 (mm/dia) GPCC','anom1979_2019JFM')

#Plotando a média de todos janeiros (1979-2019)
fig2 = plot_precip(mediajan,lats,lons,'media_precipJan981_2019 GPCC','')






