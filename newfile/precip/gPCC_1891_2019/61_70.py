import pandas as pd
import os, sys
from netCDF4 import Dataset
from function import filename2
import numpy as np
import datetime as dt  
from plot import plot_precip, plot_anom
from data_own import data_day
import matplotlib.pyplot as plt
from anomalia import anom2

#Arquivo que faz as medias de maneira manual
arquivo = 'full_data_monthly_v2020_1961_1970_025.nc'
nc_f = arquivo


#To show the file variables
nc_fid = Dataset(arquivo, 'r')  # Dataset is the class behavior to open the file
                             # and create an instance of the ncCDF4 class

lats, lons, precip, time = filename2(nc_f)

#necessidade de arrumar dt_time
dt_time = [dt.date(1931, 1, 1) + dt.timedelta(hours=t) 
           for t in time]

#print(time)
#representacao mensal (media de 3 meses)
print(dt_time)
exit()


#2020 dates
date1= dt.date(1894,1,1)
date2= dt.date(1894,2,1)
date3 = dt.date(1894,3,1)
index1 = data_day(date1,dt_time)
index2 = data_day(date2,dt_time)
index3 = data_day(date3,dt_time)
fig = plot_precip(precip[index1,:,:],lats,lons,'','')


alljanuary = []
allfebruary = []
allmarch = []
firstmonth = 0
secondmonth =1
thirdmonth = 2
for i in range(0,int(len(dt_time)/12)):
	soma_month = precip[firstmonth,:,:]
	soma_month2 = precip[secondmonth,:,:]
	soma_month3 = precip[thirdmonth,:,:]
	firstmonth = firstmonth+12
	secondmonth = secondmonth+12
	thirdmonth = thirdmonth+12
	alljanuary.append(soma_month)
	allfebruary.append(soma_month2)
	allmarch.append(soma_month3)
	
#Média de cada mês no intervalo de 1979 a 2022
mediajan = np.mean(alljanuary,axis = 0)
mediafev = np.mean(allfebruary,axis = 0)
mediamar = np.mean(allmarch,axis = 0)

#Média do trimestre de 2020
media2020 = (precip[index1,:,:]+precip[index2,:,:]+precip[index3,:,:])/3.0

#Média de todos os mesês do trimestre de 1979 a 2022
mediatotal = (mediajan+mediafev+mediamar)/3.0

#Anomalia de 2020
anom2020 = anom2(media2020,mediatotal)

#Plotando a média de Janeiro de 2020
#fig1 = plot_own(air[index1,:,:],lats,lons,'Média precipitacional de Janeiro de 2020','mediajan2020')

#Plotando a média de Fevereiro de 2020
#fig2 = plot_own(air[index2,:,:],lats,lons,'Média precipitacional de Fevereiro de 2020','mediafev2020')

#Plotando a média de Março de 2020
#fig3 = plot_own(air[index3,:,:],lats,lons,'Média precipitacional de Março de 2020','mediamar2020')

#Plotando a média do trimestre de 2020
#fig4 = plot_precip(media2020,lats,lons,'Média precipitacional do primeiro trimestre de 2020','mediatri2020')

#Plotando a média de todo trimestre de 1979 a 2022
#fig5 = plot_precip(mediatotal,lats,lons,'Média precipitacional dos trimestres de 1979 a 2022', 'mediaalltrimonths')

#Plotando a anomalia de 2020
fig6 = plot_anom(anom2020,lats,lons,'Anomalia precipitacional de 2020','anom2020')

#Média do trimestre de 2021
#media2021 = (precip[index4,:,:]+air[index5,:,:]+air[index6,:,:])/3.0

#Anomalia de 2021
anom2021 = anom2(media2021,mediatotal)

#Plotando a média de Janeiro de 2021
#fig7 = plot_own2(air[index4,:,:],lats,lons,'Média precipitacional de Janeiro de 2021','mediajan2021')

#Plotando a média de Fevereiro de 2021
#fig8 = plot_own2(air[index5,:,:],lats,lons,'Média precipitacional de Fevereiro de2 2021','mediafev2021')

#Plotando a média de Março de 2021
#fig9 = plot_own2(air[index6,:,:],lats,lons,'Média precipitacional de Março de 2021','mediamar2021')

#Plotando a média do trimestre de 2021
#fig10 = plot_precip2(media2021,lats,lons,'Média precipitacional do primeiro trimestre de 2021','mediatri2021')

#Plotando a anomalia de 2021
fig11 = plot_anom2(anom2021,lats,lons,'Anomalia precipitacional de 2021','anom2021')

plt.show()

#RESOLUÇÃO 
#LATITUDE: 180/72 = 2,5 GRAUS
#LONGITUDE: 360/144 = 2,5 GRAUS
#TENTAR ENCONTRAR DADOS COM RESOLUÇÃO MAIS BAIXA: DE 0,25 A 1 NO MÁXIMO
#SÓ DEIXAR A LINHA PRETA NO PLOT (ANOMALIA TRIMESTRAL)



