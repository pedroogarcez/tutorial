import pandas as pd
import os, sys
from netCDF4 import Dataset
from function import filename2
import numpy as np
import datetime as dt  
from plot import plot_precip, plot_anom
from data_own import data_day
import matplotlib.pyplot as plt
from anomalia import anom2

def media1921_1930():

#Arquivo que faz as medias de maneira manual
	arquivo = 'full_data_monthly_v2020_1921_1930_025.nc'
	nc_f = arquivo


#To show the file variables
	nc_fid = Dataset(arquivo, 'r')  # Dataset is the class behavior to open the file
                             # and create an instance of the ncCDF4 class

	lats, lons, precip, time = filename2(nc_f)
	precip = precip/30

#Criando o vetor dt_time que contém as datas contidas no arquivo
	dt_time = [dt.date(1921,1,1) + dt.timedelta(hours=t) 
        	   for t in time]




#Criando 3 vetores compostos somente pelos dados que pertencem aos meses de Janeiro, fevereio e Março
	alljanuary = []
	allfebruary = []
	allmarch = []
	firstmonth = 0
	secondmonth =1
	thirdmonth = 2
	for i in range(0,int(len(dt_time)/12)):
		soma_month = precip[firstmonth,:,:]
		soma_month2 = precip[secondmonth,:,:]
		soma_month3 = precip[thirdmonth,:,:]
		firstmonth = firstmonth+12
		secondmonth = secondmonth+12
		thirdmonth = thirdmonth+12
		alljanuary.append(soma_month)
		allfebruary.append(soma_month2)
		allmarch.append(soma_month3)
		
#Calculando a média desses vetores:
	mediajaneiro = np.mean(alljanuary,axis = 0)
	mediafevereiro =np.mean(allfebruary,axis = 0) 
	mediamarco =np.mean(allmarch,axis = 0) 

#Calculando a media de 1891 a 1900
	media1921_1930JFM = (mediajaneiro+mediafevereiro+mediamarco)/3.0

#Plotando a anomalia 
	#fig11 = plot_anom(anomalia2011_2019JFM,lats,lons,'anomalia2011_2019JFM','anom2011_2019JF')
	return media1921_1930JFM,dt_time


media1921_1930JFM = media1921_1930()



