
import numpy as np
import matplotlib.pyplot as plt


#lats: vetor do arquivo netCDF composto pelas latitudes 
#lons: vetor do arquivo netCDF composto pelas longitudes
#coordenada1 = dicionário composto pelas informações de interesse para plotagem (latitudes da região de estudo, longitude, título do plot, etc)
#vetor = variável de interesse para plotagem (precipitação, temperatura, vento, etc)


def local(coordenada1,coordenada2,lats,lons):
	
	lat_idx1 = np.abs(lats - coordenada1['lat']).argmin()
	lat_idx2 = np.abs(lats - coordenada2['lat']).argmin()
	lon_idx1 = np.abs(lons - coordenada1['lon']).argmin()
	lon_idx2 = np.abs(lons - coordenada2['lon']).argmin()
	
	return lat_idx1,lat_idx2,lon_idx1,lon_idx2

def medialatlon(vetor,lat_idx1,lat_idx2,lon_idx1,lon_idx2):
	
	medialat = np.mean(vetor[:,lat_idx1:lat_idx2,:],axis = 1)
	mediaquadrado = np.mean(medialat[:,lon_idx1:lon_idx2],axis = 1)
	
	return mediaquadrado
	
def plot_line(vetor1,vetor2,vetor3,coordenada):

	plt.plot(vetor1, vetor2-vetor3, c='r', marker = '', label = coordenada['label'])
	plt.ylabel(coordenada['ylabel'])
	plt.xlabel(coordenada['xlabel'])
	plt.title(coordenada['title'])
	plt.legend(loc="upper left", fontsize = 8)
	#plt.savefig('cantprecip2020',dpi =1000, bbox_inches='tight')
	plt.axhline(0.0,color = 'black', linestyle = '--')
	plt.show()

def loopmedia(dt_time,mediaquadrado):
	alljanuary = []
	allfebruary = []
	allmarch = []
	alltime = []
	alltrimestre = []
	firstmonth = 0
	secondmonth =1
	thirdmonth = 2	
	for i in range(0,int(len(dt_time)/12)):
		soma_month = mediaquadrado[firstmonth]
		soma_month2 = mediaquadrado[secondmonth]
		soma_month3 = mediaquadrado[thirdmonth]
		trime =  (mediaquadrado[firstmonth]+mediaquadrado[secondmonth]+mediaquadrado[thirdmonth])/3.0
		firstmonth = firstmonth+12
		secondmonth = secondmonth+12
		thirdmonth = thirdmonth+12
		year = dt_time[firstmonth]
		alljanuary.append(soma_month)
		allfebruary.append(soma_month2)
		allmarch.append(soma_month3)
		alltime.append(year)
		alltrimestre.append(trime)
		mediajaneiro = np.mean(alljanuary)
	mediafev = np.mean(allfebruary)
	mediamarco = np.mean(allmarch)
	mediatotalmeses = (mediajaneiro+mediafev+mediamarco)/3.0
	print(mediaquadrado)
	return alltime, alltrimestre, mediatotalmeses
	
def loopprecip(dt_time, precip,index1,index2,index3):
	alljanuary = []
	allfebruary = []
	allmarch = []
	alltime = []
	alltrimestre = []
	firstmonth = 0
	secondmonth =1
	thirdmonth = 2	
	for i in range(0,int(len(dt_time)/12)):
		soma_month = precip[firstmonth,:,:]
		soma_month2 = precip[secondmonth,:,:]
		soma_month3 = precip[thirdmonth,:,:]
		firstmonth = firstmonth+12
		secondmonth = secondmonth+12
		thirdmonth = thirdmonth+12
		alljanuary.append(soma_month)
		allfebruary.append(soma_month2)
		allmarch.append(soma_month3)
	media = (precip[index1,:,:]+precip[index2,:,:]+precip[index3,:,:])/3.0
	media1 = np.mean(alljanuary,axis = 0)
	media2 = np.mean(allfebruary,axis = 0)
	media3 = np.mean(allmarch,axis = 0)
	mediatotal = (media1+media2+media3)/3.0
	return media, mediatotal
	
def funcao_call(regiao,lats,lons):

    #Chamando a função responsável por determinar as coordenadas de nosso retângulo para cada região
    #Cantareira
    lat_idx1,lat_idx2,lon_idx1,lon_idx2 = local(região,lats,lons)
    
    #Chamando a função responsável por calcular a média do retângulo criado com a função local
    #Cantareira
    mediaquadrado = medialatlon(precip,lat_idx1,lat_idx2,lon_idx1,lon_idx2)
    #Chamando a função responsável pelos plot
    plottemporal = plot_line(alltime,alltrimestre,mediatotalmeses,cant)
    ######################3

    return plot
