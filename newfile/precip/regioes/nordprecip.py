#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import os, sys
from netCDF4 import Dataset
from function import filename2,anom
import numpy as np
import datetime as dt  
from plot import plot_precip, plot_anom, plot_tempsquare
from data_own import data_day
import matplotlib.pyplot as plt
from anomalia import anom2

#Arquivo que faz as medias de maneira manual
arquivo = 'precip.mon.mean.nc'
nc_f = arquivo

#To show the file variables
nc_fid = Dataset(arquivo, 'r')  # Dataset is the class behavior to open the file
                             # and create an instance of the ncCDF4 class
#nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)
lats, lons, air,time = filename2(nc_f)

#necessidade de arrumar dt_time
dt_time = [dt.date(1799, 1, 1) + dt.timedelta(hours=t*24) 
           for t in time]


#representacao mensal (media de 3 meses)
#print(dt_time)


#2021 dates 
date1= dt.date(2020,1,1)
date2 = dt.date(2020,2,1)
date3 = dt.date(2020,3,1)
index1 = data_day(date1,dt_time)
index2 = data_day(date2,dt_time)
index3 = data_day(date3,dt_time)

#2021 dates
date4= dt.date(2021,1,1)
date5 = dt.date(2021,2,1)
date6 = dt.date(2021,3,1)
index4 = data_day(date4,dt_time)
index5 = data_day(date5,dt_time)
index6 = data_day(date6,dt_time)



#Criando vetores compostos somente por datas referentes ao mês que gostariamos de estudar (Dezembro, Janeiro e Fevereiro [Verão 2020])

alljanuary = []
allfebruary = []
allmarch = []
firstmonth = 0
secondmonth =1
thirdmonth = 2
for i in range(0,int(len(dt_time)/12)):
	soma_month = air[firstmonth,:,:]
	soma_month2 = air[secondmonth,:,:]
	soma_month3 = air[thirdmonth,:,:]
	firstmonth = firstmonth+12
	secondmonth = secondmonth+12
	thirdmonth = thirdmonth+12
	alljanuary.append(soma_month)
	allfebruary.append(soma_month2)
	allmarch.append(soma_month3)

media2020 = (air[index1,:,:]+air[index2,:,:]+air[index3,:,:])/3.0
media2021 = (air[index4,:,:]+air[index5,:,:]+air[index6,:,:])/3.0

#Média dos trimestres no intervalo de 1979 até 2022
media1 = np.mean(alljanuary,axis = 0)
media2 = np.mean(allfebruary,axis = 0)
media3 = np.mean(allmarch,axis = 0)
mediatotal = (media1+media2+media3)/3.0
#fig1 = plot_precip(mediatotal,lats,lons,'Quartely Precipitation Average 1979-2020','mediatri1979_2022')


#Anomalia 2020
anomalia = anom2(media2020,mediatotal)
#fig2 = plot_temporal(anomalia,lats,lons,'Quartely Precipitation Average 2020','anomaliatrimestral2020')


#Anomalia 2021
anomalia2 = anom2(media2021,mediatotal)
#fig3 = plot_anom(anomalia2,lats,lons,'Quartely Precipitation Average 2021','anomaliatrimestral2021')


#Definindo as coordenadas de Amazonas
#Localização geográfica do Amazonas:  2Norte, 9Sul, 56Leste, 73Oeste
amaz = {'name': 'Região do Amazona', 'lat': -15, 'lon': 295}
amaz2 = {'name': 'Região do Amazona', 'lat': -4, 'lon': 345}


#obs: Funções utilizadas: 
#np.abs: valor absoluto: módulo: a partir do vetor lats, cria um vetor chamado lat_idx1 composto somente por valores > 0. O mesmo se aplica para o vetor longitude
#np.argmin(): Determina a posição (index) do valor que foi especificado nas variáveis cant e cant2. Logo, após criar o vetor com módulo no através da função np.abs, a função argmin procura e retorna a posição do valor atribuído em cant e can2. Vale ressaltar que a posição começa a ser contada a partir de 0 (0,1,2,3...)

#Determinando a posição de nossas latitudes específicas em nosso vetor de latitudes (lats) para Cantareira, Amazonia
lat_idx1 = np.abs(lats - amaz['lat']).argmin()
lat_idx2 = np.abs(lats - amaz2['lat']).argmin()

#Determinando a posição de nossas longitudes específicas em nosso vetor de longitudes (lons) para Cantareira, Amazonia
lon_idx1 = np.abs(lons - amaz['lon']).argmin()
lon_idx2 = np.abs(lons - amaz2['lon']).argmin()


#Baseado nas coordenadas do retangulo que foi criado acima, vamos calular as médias das latitudes e longitudes: De início possuímos um cubo com dimensoes: time, lats e lons. Ao calcular a média das latitudes, transformamos o cubo em um quadrado de dimensões longitude e time.
#Cantareira, Amazonia,
medialat = np.mean(air[:,lat_idx1:lat_idx2,:],axis = 1)

#Agora calculando a média de nosso retangulo lons x time, temos uma reta composto pelas médias das nossas coordenadas para todos nossos tempos. 
mediaquadrado = np.mean(medialat[:,lon_idx1:lon_idx2],axis = 1)



#Criando vetores compostos pela primeira media do mes de cada ano no interalo de 1979 até 2021. Será utilizado como origem de nosso plot temporal
alltime = []

#Criando vetores compostos pelos valores de precipitação de referentes a cada mês. Por exemplo, alljanuary[] corresponde ao vetor composto pelas medias de precipitação de todos os meses de Janeiro no intervalo de 1979 até 2021 no retângulo definido pelas latitudes e longitudes do Sistema Cantareira.

#Cantareira
alljanuary = []
allfebruary = []
allmarch = []
alltrimestre = []
firstmonth = 0
secondmonth =1
thirdmonth = 2

#Amazonas
alltrimeamaz = []

for i in range(0,int(len(dt_time)/12)):
	soma_month = mediaquadrado[firstmonth]
	soma_month2 = mediaquadrado[secondmonth]
	soma_month3 = mediaquadrado[thirdmonth]
	
	trime =  (mediaquadrado[firstmonth]+mediaquadrado[secondmonth]+mediaquadrado[thirdmonth])/3.0

	
	firstmonth = firstmonth+12
	secondmonth = secondmonth+12
	thirdmonth = thirdmonth+12
	year = dt_time[firstmonth]
	alljanuary.append(soma_month)
	allfebruary.append(soma_month2)
	allmarch.append(soma_month3)
	alltime.append(year)
	alltrimestre.append(trime)

	


	
#Calculando a media do vetor composto pela 
mediajaneiro = np.mean(alljanuary)
mediafev = np.mean(allfebruary)
mediamarco = np.mean(allmarch)


#print(alljanuary)
mediatotalmeses = (mediajaneiro+mediafev+mediamarco)/3.0

#Agora: plotando todos os janeiros [79:22]
#precisa fazer: média de todos os janeiros de [79:22]
#possivel resultado:mediaaaljanuary = 2.5
#mediaquadrado[::12] - mediaalljanuary

#Para plotar nosso gráfico referente a anomalia, precisamos dos seguintes parâmetros:
#plt.plot(coordenada horizontal, coordenada vertical, cor da linha, tipo de marcador,titulo)


#plt.plot(alltime, alljanuary-mediajaneiro, c='r', marker = '', label ='Janeiro')
#plt.plot(alltime, allfebruary-mediafev, c='g', marker = '', label ='Fevereiro')
#plt.plot(alltime, allmarch-mediamarco, c='b', marker = '', label = 'Março')
plotcant = plt.plot(alltime, alltrimestre-mediatotalmeses, c='k', marker = '')#, label = 'Nordeste')
#plotamaz = plt.plot(alltime, alltrimeamaz-mediatotalmeses, c='r', marker = '', label = 'Amazonia')
#print(mediaquadrado)
#print(alltime)
#print(alljanuary-mediajaneiro)

plt.ylabel('Precipitation mm/day')
plt.xlabel('Time')
plt.title('Summer Anomalia Precipitation from Nordeste')
#plt.legend(loc="upper left", fontsize = 8)
plt.savefig('nordprecip',dpi =1000, bbox_inches='tight')
plt.axhline(0.0,color = 'black', linestyle = '--')

#Diminuir as longitudes; FOcar mais nas extremidades horizontais de Sâo Paulo
#Criar novas areas para regioes: Amazonia, NOrdeste, La PLata e SUdeste


#fig = plt.figure()

#plt.plot(alltime, alltrimestre-mediatotalmeses, c='r', marker = '', label = 'Março')

#Plot da região que foi escolhida para estudo (Sistema Cantareira)
#square = plot_tempsquare(anomalia,lats,lons,'Quartely Precipitation Average 2020 mm/day','averagetrimestral2020')

plt.show()
