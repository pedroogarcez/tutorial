import pandas as pd
import os, sys
from netCDF4 import Dataset
from function import filename2,anom
import numpy as np
import datetime as dt  
from plot import plot_precip, plot_anom, plot_tempsquare
from data_own import data_day
import matplotlib.pyplot as plt
from anomalia import anom2

#Arquivo que faz as medias de maneira manual
arquivo = 'precip.mon.mean.nc'
nc_f = arquivo

#To show the file variables
nc_fid = Dataset(arquivo, 'r')  # Dataset is the class behavior to open the file
                             # and create an instance of the ncCDF4 class
#nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)
lats, lons, air,time = filename2(nc_f)

#necessidade de arrumar dt_time
dt_time = [dt.date(1799, 1, 1) + dt.timedelta(hours=t*24) 
           for t in time]


#Criando vetores compostos somente por datas referentes ao mês que gostariamos de estudar (Dezembro, Janeiro e Fevereiro [Verão 2020])

alljanuary = []
allfebruary = []
allmarch = []
firstmonth = 0
secondmonth =1
thirdmonth = 2
for i in range(0,int(len(dt_time)/12)):
	soma_month = air[firstmonth,:,:]
	soma_month2 = air[secondmonth,:,:]
	soma_month3 = air[thirdmonth,:,:]
	firstmonth = firstmonth+12
	secondmonth = secondmonth+12
	thirdmonth = thirdmonth+12
	alljanuary.append(soma_month)
	allfebruary.append(soma_month2)
	allmarch.append(soma_month3)

media2020 = (air[index1,:,:]+air[index2,:,:]+air[index3,:,:])/3.0
media2021 = (air[index4,:,:]+air[index5,:,:]+air[index6,:,:])/3.0

#Média dos trimestres no intervalo de 1979 até 2022
media1 = np.mean(alljanuary,axis = 0)
media2 = np.mean(allfebruary,axis = 0)
media3 = np.mean(allmarch,axis = 0)
mediatotal = (media1+media2+media3)/3.0
#fig1 = plot_precip(mediatotal,lats,lons,'Quartely Precipitation Average 1979-2020','mediatri1979_2022')


#Anomalia 2020
anomalia = anom2(media2020,mediatotal)
#fig2 = plot_temporal(anomalia,lats,lons,'Quartely Precipitation Average 2020','anomaliatrimestral2020')


#Anomalia 2021
anomalia2 = anom2(media2021,mediatotal)
#fig3 = plot_anom(anomalia2,lats,lons,'Quartely Precipitation Average 2021','anomaliatrimestral2021')


	
#Definindo as coordenadas de Sistema Cantareira
cant = {'name': 'Sistema Cantareira, Brazil', 'lat': -25, 'lon': 305}
cant2 = {'name': 'Sistema Cantareira, Brazil', 'lat': -19.5, 'lon': 320}
cantespecifico = {'name': 'Sistema Cantareira, Brazil', 'lat': -23.19, 'lon': 314}

#Definindo as coordenadas de Amazonas
#Localização geográfica do Amazonas:  2Norte, 9Sul, 56Leste, 73Oeste
amaz = {'name': 'Região do Amazona', 'lat': -9, 'lon': 287}
amaz2 = {'name': 'Região do Amazona', 'lat': 2, 'lon': 304}


#Definindo as coordenadas de Nordeste: 1Norte, 18Sul, 24Oeste, 48Oeste
amaz = {'name': 'Região do Nordeste', 'lat': 2, 'lon': 312}
amaz2 = {'name': 'Região do Amazona', 'lat': 9 , 'lon': 336}

#obs: Funções utilizadas: 
#np.abs: valor absoluto: módulo: a partir do vetor lats, cria um vetor chamado lat_idx1 composto somente por valores > 0. O mesmo se aplica para o vetor longitude
#np.argmin(): Determina a posição (index) do valor que foi especificado nas variáveis cant e cant2. Logo, após criar o vetor com módulo no através da função np.abs, a função argmin procura e retorna a posição do valor atribuído em cant e can2. Vale ressaltar que a posição começa a ser contada a partir de 0 (0,1,2,3...)

#Determinando a posição de nossas latitudes específicas em nosso vetor de latitudes (lats) para Cantareira, Amazonia
lat_idx1 = np.abs(lats - cant['lat']).argmin()
lat_idx2 = np.abs(lats - cant2['lat']).argmin()
lat_idxamaz1 = np.abs(lats - amaz['lat']).argmin()
lat_idxamaz2 = np.abs(lats - amaz2['lat']).argmin()


print(lat_idx1,lat_idx2)
print(lat_idxamaz1,lat_idxamaz2)


#Determinando a posição de nossas longitudes específicas em nosso vetor de longitudes (lons) para Cantareira, Amazonia
lon_idx1 = np.abs(lons - cant['lon']).argmin()
lon_idx2 = np.abs(lons - cant2['lon']).argmin()
lon_idxamaz1 = np.abs(lons - amaz['lon']).argmin()
lon_idxamaz2 = np.abs(lons - amaz2['lon']).argmin()

print(lon_idxamaz1,lon_idxamaz2)

#Determinando a posição de nossa latitude e longitude específica (de acordo com a localização da internet) nos vetores lats e lons
latespecifico = np.abs(lats - cantespecifico['lat']).argmin()
lonespecifico = np.abs(lons - cantespecifico['lon']).argmin()


#Baseado nas coordenadas do retangulo que foi criado acima, vamos calular as médias das latitudes e longitudes: De início possuímos um cubo com dimensoes: time, lats e lons. Ao calcular a média das latitudes, transformamos o cubo em um quadrado de dimensões longitude e time.
#Cantareira, Amazonia,
medialat = np.mean(air[:,lat_idx1:lat_idx2,:],axis = 1)
medialatamaz = np.mean(air[:,lat_idxamaz1:lat_idxamaz2,:],axis = 1)


#Agora calculando a média de nosso retangulo lons x time, temos uma reta composto pelas médias das nossas coordenadas para todos nossos tempos. 
mediaquadrado = np.mean(medialat[:,lon_idx1:lon_idx2],axis = 1)
mediaquadradoamaz = np.mean(medialatamaz[:,lon_idxamaz1:lon_idxamaz2],axis = 1)


#Criando vetores compostos pela primeira media do mes de cada ano no interalo de 1979 até 2021. Será utilizado como origem de nosso plot temporal
alltime = []

#Criando vetores compostos pelos valores de precipitação de referentes a cada mês. Por exemplo, alljanuary[] corresponde ao vetor composto pelas medias de precipitação de todos os meses de Janeiro no intervalo de 1979 até 2021 no retângulo definido pelas latitudes e longitudes do Sistema Cantareira.

#Cantareira
alljanuary = []
allfebruary = []
allmarch = []
alltrimestre = []
firstmonth = 0
secondmonth =1
thirdmonth = 2

#Amazonas
alltrimeamaz = []

for i in range(0,int(len(dt_time)/12)):
	soma_month = mediaquadrado[firstmonth]
	soma_month2 = mediaquadrado[secondmonth]
	soma_month3 = mediaquadrado[thirdmonth]
	soma_monthamaz = mediaquadradoamaz[firstmonth]
	soma_monthamaz2 = mediaquadradoamaz[secondmonth]
	soma_monthamaz3 = mediaquadradoamaz[thirdmonth]	
	
	trime =  (mediaquadrado[firstmonth]+mediaquadrado[secondmonth]+mediaquadrado[thirdmonth])/3.0
	trimeamaz = (mediaquadradoamaz[firstmonth]+mediaquadradoamaz[secondmonth]+mediaquadradoamaz[thirdmonth])/3.0
	
	firstmonth = firstmonth+12
	secondmonth = secondmonth+12
	thirdmonth = thirdmonth+12
	year = dt_time[firstmonth]
	alljanuary.append(soma_month)
	allfebruary.append(soma_month2)
	allmarch.append(soma_month3)
	alltime.append(year)
	
	alltrimestre.append(trime)
	alltrimeamaz.append(trimeamaz)


	
#Calculando a media do vetor composto pela 
mediajaneiro = np.mean(alljanuary)
mediafev = np.mean(allfebruary)
mediamarco = np.mean(allmarch)


#print(alljanuary)
mediatotalmeses = (mediajaneiro+mediafev+mediamarco)/3.0

#Agora: plotando todos os janeiros [79:22]
#precisa fazer: média de todos os janeiros de [79:22]
#possivel resultado:mediaaaljanuary = 2.5
#mediaquadrado[::12] - mediaalljanuary

#Para plotar nosso gráfico referente a anomalia, precisamos dos seguintes parâmetros:
#plt.plot(coordenada horizontal, coordenada vertical, cor da linha, tipo de marcador,titulo)


#plt.plot(alltime, alljanuary-mediajaneiro, c='r', marker = '', label ='Janeiro')
#plt.plot(alltime, allfebruary-mediafev, c='g', marker = '', label ='Fevereiro')
#plt.plot(alltime, allmarch-mediamarco, c='b', marker = '', label = 'Março')
plotcant = plt.plot(alltime, alltrimestre-mediatotalmeses, c='k', marker = '', label = 'Cantareira')
plotamaz = plt.plot(alltime, alltrimeamaz-mediatotalmeses, c='r', marker = '', label = 'Amazonia')
#print(mediaquadrado)
#print(alltime)
#print(alljanuary-mediajaneiro)

plt.ylabel('Precipitation mm/day')
plt.xlabel('Time')
plt.title('Summer Anomalia Precipitation')
plt.legend(loc="upper left", fontsize = 8)
plt.savefig('cantprecip2020',dpi =1000, bbox_inches='tight')
plt.axhline(0.0,color = 'black', linestyle = '--')

#Diminuir as longitudes; FOcar mais nas extremidades horizontais de Sâo Paulo
#Criar novas areas para regioes: Amazonia, NOrdeste, La PLata e SUdeste


#fig = plt.figure()

#plt.plot(alltime, alltrimestre-mediatotalmeses, c='r', marker = '', label = 'Março')

#Plot da região que foi escolhida para estudo (Sistema Cantareira)
#square = plot_tempsquare(anomalia,lats,lons,'Quartely Precipitation Average 2020 mm/day','averagetrimestral2020')

plt.show()
