
import numpy as np

def loopfunction(dt_time,mediaquadrado):
	alljanuary = []
	allfebruary = []
	allmarch = []
	alltime = []
	alltrimestre = []
	firstmonth = 0
	secondmonth =1
	thirdmonth = 2
	for i in range(0,int(len(dt_time)/12)):
		soma_month = mediaquadrado[firstmonth]
		soma_month2 = mediaquadrado[secondmonth]
		soma_month3 = mediaquadrado[thirdmonth]
		trime =  (mediaquadrado[firstmonth]+mediaquadrado[secondmonth]+mediaquadrado[thirdmonth])/3.0
		firstmonth = firstmonth+12
		secondmonth = secondmonth+12
		thirdmonth = thirdmonth+12
		year = dt_time[firstmonth]
		alljanuary.append(soma_month)
		allfebruary.append(soma_month2)
		allmarch.append(soma_month3)
		alltime.append(year)
		alltrimestre.append(trime)
	mediajaneiro = np.mean(alljanuary)
	mediafev = np.mean(allfebruary)
	mediamarco = np.mean(allmarch)
	mediatotalmeses = (mediajaneiro+mediafev+mediamarco)/3.0

	return mediatotalmes
