#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pandas as pd
import os, sys
from netCDF4 import Dataset
from function import filename2
import numpy as np
from concfunction import conc_func
import datetime as dt  
from plot import plot_precip, plot_anom
from data_own import data_day
from variablesfunction import ncdump
import matplotlib.pyplot as plt
from anomalia import anom2
from localfunction import local,medialatlon,plot_line,loopmedia, loopprecip

#Arquivo que faz as medias de maneira manual
arquivo = 'precip.mon.mean.nc'
nc_f = arquivo

#To show the file variables
nc_fid = Dataset(arquivo, 'r')  # Dataset is the class behavior to open the file
                             # and create an instance of the ncCDF4 class
#nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)
lats, lons, precip,time = filename2(nc_f)

#necessidade de arrumar dt_time
dt_time = [dt.date(1800, 1, 1) + dt.timedelta(hours=t*24) 
           for t in time]


#2021 dates 
date1= dt.date(2020,1,1)
date2 = dt.date(2020,2,1)
date3 = dt.date(2020,3,1)
index1 = data_day(date1,dt_time)
index2 = data_day(date2,dt_time)
index3 = data_day(date3,dt_time)

#2021 dates
date4= dt.date(2021,1,1)
date5 = dt.date(2021,2,1)
date6 = dt.date(2021,3,1)
index4 = data_day(date4,dt_time)
index5 = data_day(date5,dt_time)
index6 = data_day(date6,dt_time)


#PLOT GLOBAL
#Chamando a função responsável por calcular a média das precipitaçãoes em todas as latitudes e longitudes contidas no arquivo
#Para o ano de 2020
media2020,mediatotal = loopprecip(dt_time,precip,index1,index2,index3)

#Para o ano de 2021
media2021,mediatotal = loopprecip(dt_time,precip,index4,index5,index6)

#Plotando a media para o intervalo de 1979-2020
#fig1 = plot_precip(mediatotal,lats,lons,'Quartely Precipitation Average 1979-2020','mediatri1979_2022')


#Anomalia 2020
anomalia = anom2(media2020,mediatotal)
#fig2 = plot_anom(anomalia,lats,lons,'Quartely Precipitation Average 2020','anomaliatrimestral2020')

#Anomalia 2021
anomalia2 = anom2(media2021,mediatotal)
#fig3 = plot_anom(anomalia2,lats,lons,'Quartely Precipitation Average 2021','anomaliatrimestral2021')


################


#PLOT TEMPORAL
#Chamando a função responsável por calcular a média do quadrado que defininmos
alltime, alltrimestre, mediatotalmeses = loopmedia(dt_time,precip)


#Definindo as coordenadas da região de estudo no Sistema Cantareira

#Definindo as coordenadas da região de estudo no Sistema Cantareira
regiao1 = {'name': 'Sistema Cantareira, Brazil', 'lat': -25, 'lon': 305, 'ylabel': 'JFM precipitation anomaly (mm/day)', 'xlabel': 'Time','label':'Precipation','title':'JFM Rainfall anomaly in Cantareira System'}

plot=funcao_call(regiao)

######################3

def funcao_call(regiao,lats,lons)

    #Chamando a função responsável por determinar as coordenadas de nosso retângulo para cada região
    #Cantareira
    lat_idx1,lat_idx2,lon_idx1,lon_idx2 = local(region,lats,lons)
    
    #Chamando a função responsável por calcular a média do retângulo criado com a função local
    #Cantareira
    mediaquadrado = medialatlon(precip,lat_idx1,lat_idx2,lon_idx1,lon_idx2)
    #Chamando a função responsável pelos plot
    plottemporal = plot_line(alltime,alltrimestre,mediatotalmeses,cant)
    ######################3

    return plote


######################3

#Definindo as coordenadas da região de estudo no Amazonas
amaz = {'name': 'Região do Amazona', 'lat': -15, 'lon': 275,'ylabel': 'JFM precipitation anomaly (mm/day)','xlabel':'Time','label':'Precipitaion','title':'JFM Rainfall anomaly in Amazonas'}
amaz2 = {'name': 'Região do Amazona', 'lat': 0, 'lon': 325}

#Amazonas
lat_idxamaz1,lat_idxamaz2,lon_idxamaz1,lon_idxamaz2 = local(amaz,amaz2,lats,lons)

#Amazonas
mediaquadradoamaz = medialatlon(precip,lat_idxamaz1,lat_idxamaz2,lon_idxamaz1,lon_idxamaz2)

#Chamando a função responsável por criar os vetores necessários para plotagem
#Cantareira
alltime, alltrimestre, mediatotalmeses = loopmedia(dt_time,mediaquadradocant)
#Amazonas
alltime,alltrimestreamaz,mediatotalmesesamaz = loopmedia(dt_time,mediaquadradoamaz)

plttemporalamazonia = plot_line(alltime,alltrimestreamaz,mediatotalmesesamaz,amaz)

######################3

