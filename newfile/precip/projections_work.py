import pandas as pd
import os, sys
from netCDF4 import Dataset
from function import filename2,anom
import numpy as np
from concfunction import conc_func
import datetime as dt  
from plotprojection import plot_own,plot_own_ortho, plot_own_robin
from data_own import data_day
from variablesfunction import ncdump
import matplotlib.pyplot as plt



#Arquivo que faz as medias de maneira manual
arquivo = 'precip.mon.mean.nc'
nc_f = arquivo

#To show the file variables
nc_fid = Dataset(arquivo, 'r')  # Dataset is the class behavior to open the file
                             # and create an instance of the ncCDF4 class
#nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)
lats, lons, air,time = filename2(nc_f)

#necessidade de arrumar dt_time
dt_time = [dt.date(1800, 1, 1) + dt.timedelta(hours=t*24) 
           for t in time]
#print(dt_time)
#representacao mensal (media de 3 meses)



date_start1= dt.date(2021,1,1)
date_finish1 = dt.date(2021,1,1)

date_start2= dt.date(2021,1,1)
date_finish2 = dt.date(2020,3,1)

date_start3= dt.date(2021,1,1)
date_finish3 = dt.date(2021,3,1)


index1 = data_day(date_start1,dt_time)
index2= data_day(date_finish1,dt_time)
index3 = data_day(date_start2,dt_time)
index4 = data_day(date_finish2,dt_time)
index5 = data_day(date_start3,dt_time)
index6 = data_day(date_finish3,dt_time)

#ok
#fig1=plot_own(air[index1,:,:],lats,lons,'','')
#ok
#fig2=plot_own_ortho(air[index1,:,:],lats,lons,'','')
#ok, mas nao funciona colocando o brasil no centro
fig3=plot_own_robin(air[index1,:,:],lats,lons,'','')

plt.show()
exit()


monthmedia1=np.mean(air[index1:index2,:,:],axis=0)
monthmedia2=np.mean(air[index3:index4,:,:],axis=0)
monthmedia3=np.mean(air[index5:index6,:,:],axis=0)


suma_air=np.zeros([len(lats),len(lons)])

#suma_air[:,:] = suma_air[:,:] + air[i,:,:]

media = (monthmedia1+monthmedia2+monthmedia3)/3.0


#plot_own(media,lats,lons,'Manual average Jan:Mar 19:21','Manual average Jan:Mar 19:21')
#plt.savefig('Manual average Jan:Mar 19:21')

concmonth=[]

#concmonth = [monthmedia1,monthmedia2,monthmedia3]
concmonth.append(monthmedia1)
concmonth.append(monthmedia2)
mediaconc = np.mean(concmonth,axis=0)

#print(mediaconc)

fig1=plot_own(mediaconc,lats,lons,'','')

fig2=plot_own_robin(mediaconc,lats,lons,'','')

plt.show()



