from netCDF4 import Dataset 
import numpy as np

def filename(fn):
	nc_fid = Dataset(fn, 'r')  # Dataset is the class behavior to open the file
                             # and create an instance of the ncCDF4 class
		#Print the descripsion of the data, optional. 
	#nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)

#The  above command shown that there are 4 variables
#in the nc_f = ./air.sig995.2012.nc file.  
#The data shown the air temperature of 1 year. 
# Extract data from NetCDF file
	lats = nc_fid.variables['lat'][:]  # extract/copy the data
	lons = nc_fid.variables['lon'][:]
	time = nc_fid.variables['time'][:]
	air  = nc_fid.variables['air_dep'][:]  # shape is time, lat, lon as shown above

	return lats, lons, air, time
	
def fe(fn):
	nc_fid = Dataset(fn, 'r')  # Dataset is the class behavior to open the file
                             # and create an instance of the ncCDF4 class
	
	#Print the descripsion of the data, optional. 
	#nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)

#The  above command shown that there are 4 variables
#in the nc_f = ./air.sig995.2012.nc file.  
#The data shown the air temperature of 1 year. 
# Extract data from NetCDF file
	lats = nc_fid.variables['lat'][:]  # extract/copy the data
	lons = nc_fid.variables['lon'][:]
	time = nc_fid.variables['time'][:]
	air  = nc_fid.variables['air'][:]  # shape is time, lat, lon as shown above

	return lats, lons, air, time
	
def local (lat,lon,lats,lons):
	
	cp = {'name': 'Cachoeira Paulista, Brazil', 'lat': lat, 'lon': lon}
	
	
# Find the nearest latitude and longitude for Darwin
	lat_idx = np.abs(lats - cp['lat']).argmin()
	lon_idx = np.abs(lons - cp['lon']).argmin()
	
	return lat_idx,lon_idx
	
	
def local_2(po,lats,lons):
	lat_idx = np.abs(lats - po['lat']).argmin()
	lon_idx = np.abs(lons - po['lon']).argmin()
	return lat_idx,lon_idx
	
	
	
def ncdump(nc_fid, verb=True):
    '''
    ncdump outputs dimensions, variables and their attribute information.
    The information is similar to that of NCAR's ncdump utility.
    ncdump requires a valid instance of Dataset.

    Parameters
    ----------
    nc_fid : netCDF4.Dataset
        A netCDF4 dateset object
    verb : Boolean
        whether or not nc_attrs, nc_dims, and nc_vars are printed

    Returns
    -------
    nc_attrs : list
        A Python list of the NetCDF file global attributes
    nc_dims : list
        A Python list of the NetCDF file dimensions
    nc_vars : list
        A Python list of the NetCDF file variables
    '''
    def print_ncattr(key):
        """
        Prints the NetCDF file attributes for a given key

        Parameters
        ----------
        key : unicode
            a valid netCDF4.Dataset.variables key
        """
        try:
            print ("\t\ttype:", repr(nc_fid.variables[key].dtype))
            for ncattr in nc_fid.variables[key].ncattrs():
                print ('\t\t%s:' % ncattr,\
                      repr(nc_fid.variables[key].getncattr(ncattr)))
        except KeyError:
            print ("\t\tWARNING: %s does not contain variable attributes" % key)

    # NetCDF global attributes
    nc_attrs = nc_fid.ncattrs()
    if verb:
        print ("NetCDF Global Attributes:")
        for nc_attr in nc_attrs:
            print ('\t%s:' % nc_attr, repr(nc_fid.getncattr(nc_attr)))
    nc_dims = [dim for dim in nc_fid.dimensions]  # list of nc dimensions
    # Dimension shape information.
    if verb:
        print ("NetCDF dimension information:")
        for dim in nc_dims:
            print ("\tName:", dim) 
            print ("\t\tsize:", len(nc_fid.dimensions[dim]))
            print_ncattr(dim)
    # Variable information.
    nc_vars = [var for var in nc_fid.variables]  # list of nc variables
    if verb:
        print( "NetCDF variable information:")
        for var in nc_vars:
            if var not in nc_dims:
                print ('\tName:', var)
                print ("\t\tdimensions:", nc_fid.variables[var].dimensions)
                print ("\t\tsize:", nc_fid.variables[var].size)
                print_ncattr(var)
    return nc_attrs, nc_dims, nc_vars
	
	



'''
def quadrado(p,y6,iz):
	xquadrado=p*p
	yquadrado=y6*y6
	zquadrado=iz**2
	return xquadrado,yquadrado, zquadrado


def numero(p):
	num=p**2
	return num	
	
	
def numero2(y)
	num2=y**2
	return num2
	
	
def calculadora(p)
	xquadrado=x**x
	'''

