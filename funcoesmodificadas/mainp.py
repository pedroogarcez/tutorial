'''
NAME
    NetCDF with Python
PURPOSE
    To demonstrate how to read and write data with NetCDF files using
    a NetCDF file from the NCEP/NCAR Reanalysis.
    Plotting using Matplotlib and Basemap is also shown.
PROGRAMMER(S)
    Chris Slocum
    Jhonatan M. 
REVISION HISTORY
    20140320 -- Initial version created and posted online
    20140722 -- Added basic error handling to ncdump
                Thanks to K.-Michael Aye for highlighting the issue
    20210114 -- Statistical work with the data
                Jhonatan  
REFERENCES
    netcdf4-python -- http://code.google.com/p/netcdf4-python/
    NCEP/NCAR Reanalysis -- Kalnay et al. 1996
        http://dx.doi.org/10.1175/1520-0477(1996)077<0437:TNYRP>2.0.CO;2
'''

# Python standard library datetime  module
import datetime as dt  
# http://code.google.com/p/netcdf4-python/
import numpy as np

from netCDF4 import Dataset 

import matplotlib.pyplot as plt

#from  read_ncfiles import ncdump

#To import the library necessary to make the maps 
from mpl_toolkits.basemap import Basemap, addcyclic, shiftgrid

from data_own import data_day

from plot import plot_own

from function import filename,fe

from function2 import mean,meanp

#forma diferente de importar a função
#import function2 as io



'''
Seasons beginning
Autumm = [(2012,3,20) - (2012,6,20)]
Winter = [(2012,6,20) - (2012,9,22)]
Spring = [(2012,9,20) - (2012,12,21)]
Summer = [(2012,12,21) - (2012,12,31)]
'''

# ../original = retornando uma pasta que já contenha o arquivo 
nc_f = '../original/air.sig995.2012.nc'
nc = '../original/air.departure.sig995.2012.nc'

lats,lons,air,time=fe(nc_f)
lats2,lons2,air2,time2=filename(nc)


#forma diferente de chamar a funçao
#io.mean([])
#io.meanp([])

#2. Creating the time vector of the data. 
############################################
# Select some  random day in 2012
time_idx = 237 



#The calendar of reanalysis is out of phase of two days respect

offset = dt.timedelta(hours=48)

# List of all times in the file as datetime objects

dt_time = [dt.date(1, 1, 1) + dt.timedelta(hours=t) - offset\
           for t in time]

dt_time2 = [dt.date(1, 1, 1) + dt.timedelta(hours=t) - offset\
           for t in time2]

cur_time = dt_time[time_idx]

#My current day 
summer_beginning=dt.date(2012,1,1)
summer_finish=dt.date(2012,3,20)
autumm_beginning=dt.date(2012,3,20)
autumm_finish=dt.date(2012,6,20)
winter_beginning=dt.date(2012,6,20)
winter_finish=dt.date(2012,9,22)
spring_beginning=dt.date(2012,9,22)
spring_finish=dt.date(2012,12,21)


#To return 1 variable in air (nc_f) nc_f=air.sig995.2012.nc
#To find the index of my data in the vector of time
index1=data_day(summer_beginning,dt_time)
index2=data_day(summer_finish,dt_time)
index3=data_day(autumm_beginning,dt_time)
index4=data_day(autumm_finish,dt_time)
index5=data_day(winter_beginning,dt_time)
index6=data_day(winter_finish,dt_time)
index7=data_day(spring_beginning,dt_time)
index8=data_day(spring_finish,dt_time)


#To return 1 variable in air_dep (nc) nc=air.departure.sig995.2012.nc
index9=data_day(summer_beginning,dt_time2)
index10=data_day(summer_finish,dt_time2)
index11=data_day(autumm_beginning,dt_time2)
index12=data_day(autumm_finish,dt_time2)
index13=data_day(winter_finish,dt_time2)
index14=data_day(winter_finish,dt_time2)
index15=data_day(spring_finish,dt_time2)
index16=data_day(spring_finish,dt_time2)



media3=mean(index1,index2,air,lats,lons)
media4=mean(index3,index4,air,lats,lons)
media5=meanp(air[index1:index2,:,:])





#Loop to calculate the average temperature in nc_f = './air.sig995.2012.nc
#1) matriz np.zeros com duas dimensões: lats x lons
suma_air=np.zeros([len(lats),len(lons)])
#2) Definindo i como a variavel tempo e determinando o intervalo de tempo 
for i in range(index3,index4):
	suma_air[:,:] = suma_air[:,:] + air[i,:,:]
media = suma_air[:,:]/(index4-index3)


#Loop to calculate the average temperature in nc = 'air.departure.sig995.2012.nc'
#1) matriz np.zeros com duas dimensões: lats x lons
suma_air2=np.zeros([len(lats),len(lons)])
#2) Definindo i como a variavel tempo e determinando o intervalo de tempo 
for i in range(index11,index12):
	suma_air[:,:] = suma_air2[:,:] + air[i,:,:]
media2 = suma_air[:,:]/(index12-index11)

#média de cada estação pros 2 arquivos
#media_p = np.mean(air[index1:index2,:,:],axis=0)
#plot_own(media_p,lats,lons,'','')
#media = np.mean(air[index3:index4,:,:],axis=0)
#plot_own(media,lats,lons,'Average Autumm', 'mmm')

'''
#Argumentos plot_own: datatoplot,lats,lons,plotname,figname
#No return
plot_own(air[index1,:,:],lats,lons,' average','')
plot_own(air2[index9,:,:],lats2,lons2,' average','')
'''
plot_own(media3,lats,lons,'Media','Media')
plot_own(media4,lats,lons,'Media2','Media2')
plot_own(media5,lats,lons,'Media2','Media2')






print(media)
#plt.show()



