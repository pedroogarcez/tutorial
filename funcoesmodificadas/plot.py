
import numpy  as np 

import matplotlib.pyplot as plt

#from  read_ncfiles import ncdump

#To import the library necessary to make the maps 

from mpl_toolkits.basemap import Basemap, addcyclic, shiftgrid

def plot_own(datatoplot,lats,lons,plotname,figname):

	#3)To plot the medium temperature in Autumm 2012.  ################################################################### #open figure fig = plt.figure() #Adjust the location of the interior of the figgure fig.subplots_adjust(left=0., right=1., bottom=0., top=0.9) # Setup the map. See http://matplotlib.org/basemap/users/mapsetup.html # for other projections.  #proj = ccrs.PlateCarree() 
	#proj = 'moll'
	
	proj  = 'cyl'
	
	#Define latitudes to plot (-90,90)
	
	lat_i =  -80.0 
	lat_f =   20.0
	
	#Define longitudes  to plot (0,360)
	lon_i =  250.0 
	lon_f =  360.0
	
	m = Basemap(projection=proj, llcrnrlat=lat_i, urcrnrlat=lat_f,\
	            llcrnrlon=lon_i, urcrnrlon=lon_f, resolution='c', lon_0=0)
	
	#m = Basemap(projection='moll', llcrnrlat=-90, urcrnrlat=90,\
	#            llcrnrlon=0, urcrnrlon=360, resolution='c', lon_0=0)
	
	m.drawcoastlines()
	m.drawmapboundary()
	
	# Make the plot continuous
	air_cyclic, lons_cyclic = addcyclic(datatoplot, lons[:])
	# Shift the grid so lons go from -180 to 180 instead of 0 to 360.
	#air_cyclic, lons_cyclic = shiftgrid(180., air_cyclic, lons_cyclic, start=False)
	# Create 2D lat/lon arrays for Basemapnc_f = './air.sig995.2012.nc'  # Your filename
	lon2d, lat2d = np.meshgrid(lons_cyclic, lats[:])
	# Transforms lat/lon into plotting coordinates for projection
	x, y = m(lon2d, lat2d)
	# Plot of air temperature with 11 contour intervals
	cs = m.contourf(x, y, air_cyclic, 11, cmap=plt.cm.Spectral_r)
	cbar = plt.colorbar(cs, orientation='horizontal', shrink=0.5)
	#cbar.set_label("%s (%s)" % (nc_fid.variables['air'].var_desc,\
	#                            nc_fid.variables['air'].units))
	#plt.title("%s on %s" % (nc_fid.variables['air'].var_desc, cur_time))
	plt.title("%s"%(plotname))
	
	plt.savefig('%s'%(figname), dpi=1000)
	
	
	
	#média de cada estação 
	#média anual 
	plt.show()
	
	
	return 
	
def plot_temporal(time,variable):

	plt.plot(time,variable)
	plt.title('Average anual temperature in Cachoeira Paulista')
	plt.savefig('iii')
	plt.show()
	return
	
	
#Pedro function
def plot_temporal2(time,constante,timevector,variableft):
	plt.plot(timevector,variableft,'r-')
	plt.plot(time, constante, c='b', marker='o')
	plt.title('Pointer function')
	plt.show()
	return




#Jhonathan function
def plot_temporal3(time,variabel,idex_t):
	plt.plot(time,variabel,'r-')
	plt.plot(time[idex_t], variabel[idex_t], c='b', marker='o')
	plt.title('Pointer function')
	plt.show()
	return
	
#def plot_pointer(time,variable,c,marker)

	
	#return
	       
