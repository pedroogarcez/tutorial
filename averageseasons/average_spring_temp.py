'''
NAME
    NetCDF with Python
PURPOSE
    To demonstrate how to read and write data with NetCDF files using
    a NetCDF file from the NCEP/NCAR Reanalysis.
    Plotting using Matplotlib and Basemap is also shown.
PROGRAMMER(S)
    Chris Slocum
    Jhonatan M. 
REVISION HISTORY
    20140320 -- Initial version created and posted online
    20140722 -- Added basic error handling to ncdump
                Thanks to K.-Michael Aye for highlighting the issue
    20210114 -- Statistical work with the data
                Jhonatan  
REFERENCES
    netcdf4-python -- http://code.google.com/p/netcdf4-python/
    NCEP/NCAR Reanalysis -- Kalnay et al. 1996
        http://dx.doi.org/10.1175/1520-0477(1996)077<0437:TNYRP>2.0.CO;2
'''

# Python standard library datetime  module
import datetime as dt  
# http://code.google.com/p/netcdf4-python/
import numpy as np

from netCDF4 import Dataset 

import matplotlib.pyplot as plt

#from  read_ncfiles import ncdump

#To import the library necessary to make the maps 
from mpl_toolkits.basemap import Basemap, addcyclic, shiftgrid

'''
Seasons beginning
Autumm = [(2012,3,20) - (2012,6,20)]
Winter = [(2012,6,20) - (2012,9,22)]
Spring = [(2012,9,20) - (2012,12,21)]
Summer = [(2012,12,21) - (2012,12,31)]
'''

#1. Load data: carregando os dados para abrir o arquivo
#############################################################################
#File location to be loaded 
nc_f = '../original/air.sig995.2012.nc'  # Your filename

#Load file. #DÚVIDA 
nc_fid = Dataset(nc_f, 'r')  # Dataset is the class behavior to open the file
                             # and create an instance of the ncCDF4 class

#Print the descripsion of the data, optional. 
#nc_attrs, nc_dims, nc_vars = ncdump(nc_fid)

#The  above command shown that there are 4 variables
#in the nc_f = ./air.sig995.2012.nc file.  
#The data shown the air temperature of 1 year. 
# Extract data from NetCDF file
lats = nc_fid.variables['lat'][:]  # extract/copy the data
lons = nc_fid.variables['lon'][:]
time = nc_fid.variables['time'][:]
air  = nc_fid.variables['air'][:]  # shape is time, lat, lon as shown above


#2. Creating the time vector of the data. 
############################################
# Select some  random day in 2012
time_idx = 237 


#The calendar of reanalysis is out of phase of two days respect 
# to python date calendar.
# Python and the renalaysis are slightly off in time so this fixes that problem
offset = dt.timedelta(hours=48)

# List of all times in the file as datetime objects

#from netcdf imformation: time > hours since 1-1-1 00:00:0.0
#Which means that the data beging from year=1, moth=1, and hour=1:: 1/1/1

#class datetime.date(year, month, day)
#1forma
dt_time = [dt.date(1, 1, 1) + dt.timedelta(hours=t) - offset\
           for t in time]

#2forma
#the above line is the same this loop
#
#for t in time: 
#    dt_time = dt.date(1, 1, 1) + dt.timedelta(hours=t) - offset

#3forma
#dt_time=np.zeros(len(time))
#for i in range(0,len(time)):
	#dt_time[i] = dt.date(1,1,1) + dt.timedelta(hours=time[i]) - offset

#Current time 
cur_time = dt_time[time_idx]

#My current day 
spring_beginning=dt.date(2012,9,22)
spring_finish=dt.date(2012,12,21)

#To find the index of my data in the vector of time
index1=0

for i in range(0,len(dt_time)): 
    if(spring_beginning==dt_time[i]):
        print('index=%s'%(i),spring_beginning)
        index=i
        break
        
index2 = -1000
for i in range(0,len(dt_time)):
	if(spring_finish==dt_time[i]):
		print('index', spring_finish)
		index2=i
		break

soma = 0
''' To realize the average in one dimension
for i in range(index1,index2):
	soma = soma + air[i,1,1]
media = soma/(index2-index)
print(soma,media)
'''
suma_air=np.zeros([len(lats),len(lons)])
for i in range(index1,index2):
	suma_air[:,:] = suma_air[:,:] + air[i,:,:]
#media = suma_air[:,:]/(index2-index1)
media_p = np.mean(air[index1:index2,:,:],axis=0)
print(media_p)


#3)To plot the medium temperature in Autumm 2012.
###################################################################

#open figure
fig = plt.figure()
#Adjust the location of the interior of the figgure
fig.subplots_adjust(left=0., right=1., bottom=0., top=0.9)

# Setup the map. See http://matplotlib.org/basemap/users/mapsetup.html
# for other projections.


#proj = ccrs.PlateCarree() 
#proj = 'moll'

proj  = 'cyl'

#Define latitudes to plot (-90,90)

lat_i =  -90
lat_f =   90

#Define longitudes  to plot (0,360)
lon_i =  0 
lon_f =  360.0

m = Basemap(projection=proj, llcrnrlat=lat_i, urcrnrlat=lat_f,\
            llcrnrlon=lon_i, urcrnrlon=lon_f, resolution='c', lon_0=0)

#m = Basemap(projection='moll', llcrnrlat=-90, urcrnrlat=90,\
#            llcrnrlon=0, urcrnrlon=360, resolution='c', lon_0=0)

m.drawcoastlines()
m.drawmapboundary()
m.drawparallels(np.arange(-90.,90.,30.),labels=[1,0,0,0]) # draw parallels
m.drawmeridians(np.arange(-180.,180.,60.),labels=[0,0,0,1]) # draw meridians

# Make the plot continuous
air_cyclic, lons_cyclic = addcyclic(media_p[:,:], lons[:])
# Shift the grid so lons go from -180 to 180 instead of 0 to 360.
#air_cyclic, lons_cyclic = shiftgrid(180., air_cyclic, lons_cyclic, start=False)
# Create 2D lat/lon arrays for Basemap
lon2d, lat2d = np.meshgrid(lons_cyclic, lats[:])
# Transforms lat/lon into plotting coordinates for projection
x, y = m(lon2d, lat2d)
# Plot of air temperature with 11 contour intervals
cs = m.contourf(x, y, air_cyclic, 11, cmap=plt.cm.Spectral_r)
cbar = plt.colorbar(cs, orientation='horizontal', shrink=0.5)
cbar.set_label("%s (%s)" % (nc_fid.variables['air'].var_desc,\
                            nc_fid.variables['air'].units))
#plt.title("%s on %s" % (nc_fid.variables['air'].var_desc, cur_time))
plt.title("Average Spring 2020 Temperature")

plt.savefig('Average Spring 2020 Temperature', dpi=1000)



#média de cada estação 
#média anual 


plt.show()
exit()
